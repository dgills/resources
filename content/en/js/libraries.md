---
title: Libraries
description: We're curating useful resources about Javascript libraries from around the Internet to help you on your software journey!
position: 3003
category: Javascript
---



## React

React is a declarative, efficient, and flexible JavaScript library for building user interfaces.
It is a little JavaScript library with a big influence over the webdev world.

Developed by Facebook, this little JavaScript library has had a massive influence on the history of web development.

<youtube-video id="Tn6-PIqc4UM?list=PL0vfts4VzfNiI1BsIK5u7LpPaIDKMJIDN"> </youtube-video>

## jQuery

jQuery is a JavaScript library that allows web developers to add extra functionality to their websites. It is the world's most popular JavaScript library (by far) in terms of sites using it in production today.

<youtube-video id="UU-GebNqdbg?list=PL0vfts4VzfNiI1BsIK5u7LpPaIDKMJIDN"> </youtube-video>


## PostCSS

PostCSS is a tool that allows you to use modern CSS features like nesting and mixins, while supporting legacy browsers.

This video will help you learn why this simple JavaScript library is one of the most popular tools in the web development industry. 

<youtube-video id="WhCXiEwdU1A"> </youtube-video>

---
title: Assets
description: We're curating useful JavaScript assets from around the Internet to help you on your software journey!
position: 3010
category: Javascript
---

## Node.js Development Tool

This is a flexible open-source Node.js app development tool. It helps you build production-ready Node.js backend without wasting time on repetative coding.

[![Node.js Development Tool Preview](https://i.imgur.com/NEQVzBk.png)](https://amplication.com/)

[Author : amplication.com](https://amplication.com/)

<cta-button text="View" link="https://amplication.com/"></cta-button>
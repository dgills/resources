---
title: Books
description: We're compiling useful resources around the internet to help you on create video games!
position: 10001
category: Video Games Development
---

## Game Development

### 3D Math Primer for Graphics and Game Development 

<book-cover link="https://gamemath.com/book/intro.html" img-src="https://i.imgur.com/5ikJm7W.jpg" alt="Book cover for 3D Math Primer for Graphics and Game Development "> </book-cover>

This book is about 3D math, the geometry and algebra of 3D space. It is designed to teach you how to describe objects and their positions, orientations, and trajectories in 3D using mathematics. This is not a book about computer graphics, simulation, or even computational geometry, although if you plan on studying those subjects, you will definitely need the information here.

<cta-button text="Complete Book" link="https://gamemath.com/book/intro.html"></cta-button>

### Designing Virtual Worlds 

<book-cover link="https://mud.co.uk/richard/DesigningVirtualWorlds.pdf" img-src="https://i.imgur.com/H73zkQP.jpg" alt="Book cover for Designing Virtual Worlds "> </book-cover>

Designing Virtual Worlds is the most comprehensive treatment of virtual world design to-date from one of the true pioneers and most sought-after design consultants. It's a tour de force of VW design, stunning in intellectual scope, spanning the literary, economic, sociological, psychological, physical, technological, and ethical underpinnings of design, while providing the reader with a deep, well-grounded understanding of VW design principles. It covers everything from MUDs to MOOs to MMORPGs, from text-based to graphical VWs.

<cta-button text="Complete Book" link="https://mud.co.uk/richard/DesigningVirtualWorlds.pdf"></cta-button>

### Game AI Pro 

<book-cover link="http://www.gameaipro.com/" img-src="https://i.imgur.com/ICTg8Y5.jpg" alt="Book cover for Game AI Pro"> </book-cover>

Game AI Pro: Collected Wisdom of Game AI Professionals presents state-of-the-art tips, tricks, and techniques drawn from developers of shipped commercial games as well as some of the best-known academics in the field. This book acts as a toolbox of proven techniques coupled with the newest advances in game AI. These techniques can be applied to almost any game and include topics such as behavior trees, utility theory, path planning, character behavior, and tactical reasoning.

<cta-button text="Complete Book" link="http://www.gameaipro.com/"></cta-button>

</br>

<cta-button text="More Books On Game Development" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#web-performance"></cta-button>
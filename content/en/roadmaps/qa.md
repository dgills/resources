---
title: Quality Assurance 
description: We're compiling a library of useful roadmaps in the domain of Quality Assurance from around the Internet to help you on your software journey!
position: 206
category: Roadmaps
---

## 2022

### Quality Assurance Engineer Roadmap

Steps to follow in order to become a modern QA Engineer

[![Quality Assurance Engineer Roadmap 2022](https://i.imgur.com/yCdxxU0.png)](https://roadmap.sh/qa)

[Source : Kamran Ahmed - GitHub](https://github.com/kamranahmedse)

<cta-button text="View Roadmap" link="https://roadmap.sh/qa"></cta-button>


## 2021

### Quality Assurance Roadmap

[![Quality Assurance Roadmap 2021](https://i.imgur.com/BY2JSAz.jpg)](https://github.com/fityanos/awesome-quality-assurance-roadmap)

[Source : Anas Fitiani - GitHub](https://github.com/fityanos)

<cta-button text="View Roadmap" link="https://github.com/fityanos/awesome-quality-assurance-roadmap"></cta-button>
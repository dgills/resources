---
title: Books
description: We're compiling a library of useful Computer Organization and Architecture books from around the Internet to help you on your software journey!
position: 16000
category: Computer Organization and Architecture
---

## Computer Organization and Architecture

### Basic Computer Architecture 

<book-cover link="https://www.cse.iitd.ac.in/~srsarangi/archbook/archbook.pdf" img-src="https://i.imgur.com/KWvauYD.png" alt="Book cover for Basic Computer Architecture"> </book-cover>

Computer architecture is the study of computers. In this book you will learn basic design principles of computers including the basic technologies, algorithms, design methodologies and future trends.

<cta-button text="Download PDF" link="https://www.cse.iitd.ac.in/~srsarangi/archbook/archbook.pdf"></cta-button>


### Computer Organization and Design Fundamentals 

<book-cover link="https://faculty.etsu.edu/tarnoff/138292/" img-src="https://i.imgur.com/2MOEAAr.png" alt="Book cover for Computer Organization and Design Fundamentals "> </book-cover>

Computer Organization and Design Fundamentals takes the reader from the basic design principles of the modern digital computer to a top-level examination of its architecture. This book can serve either as a textbook to an introductory course on computer hardware or as the basic text for the aspiring geek who wants to learn about digital design.

<cta-button text="Complete Book" link="https://faculty.etsu.edu/tarnoff/138292/"></cta-button>

</br>

<cta-button text="More Books On Computer Organization and Architecture" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#computer-organization-and-architecture"></cta-button>


## Compiler Design 

### Basics of Compiler Design 

<book-cover link="http://hjemmesider.diku.dk/~torbenm/Basics//basics_lulu2.pdf" img-src="https://i.imgur.com/XsTru5d.jpg" alt="Book cover for Basics of Compiler Design "> </book-cover>

This textbook is intended for an introductory course on Compiler Design, suitable for use in an undergraduate programme in computer science or related fields.

It presents techniques for making realistic, though non-optimizing compilers for simple programming languages using methods that are close to those used in "real" compilers, albeit slightly simplified in places for presentation purposes. All phases required for translating a high-level language to machine language is covered, including lexing, parsing, intermediate-code generation, machine-code generation and register allocation.

<cta-button text="Download PDF" link="http://hjemmesider.diku.dk/~torbenm/Basics//basics_lulu2.pdf"></cta-button>

<cta-button text="Exercise Solutions" link="http://hjemmesider.diku.dk/~torbenm/Basics//solutions.pdf"></cta-button>


### Compiler Design: Theory, Tools, and Examples

<book-cover link="http://elvis.rowan.edu/~bergmann/books/Compiler_Design/c_cpp/Text/C_CppEd.pdf" img-src="https://i.imgur.com/Dbg8TGn.gif" alt="Book cover for Compiler Design: Theory, Tools, and Examples"> </book-cover>

This is an introductory level text for compiler design courses, that emphasizes problem solving skills. The concepts are clearly presented with sampler problems and diagrams to illustrate the concepts. The text also covers lex and yacc two compiler generating tools in UNIX.

<cta-button text="C/C++ Edition " link="http://elvis.rowan.edu/~bergmann/books/Compiler_Design/c_cpp/Text/C_CppEd.pdf"></cta-button>

<cta-button text="Java Edition" link="http://elvis.rowan.edu/~bergmann/books/Compiler_Design/java/CompilerDesignBook.pdf"></cta-button>


### Let's Build a Compiler 

<book-cover link="http://www.stack.nl/~marcov/compiler.pdf" img-src="https://i.imgur.com/5s1e4ee.jpg" alt="Book cover for Let's Build a Compiler "> </book-cover>

This book is a tutorial on the theory and practice of developing language parsers and compilers from scratch. Before you are finished, you will have covered every aspect of compiler construction, designed a new programming language, and built a working compiler.

At the end of this series you will by no means be a computer scientist, nor will you know all the esoterics of compiler theory. The author intended to completely ignore the more theoretical aspects of the subject. What you will know is all the practical aspects that one needs to know to build a working system

<cta-button text="Download PDF" link="http://www.stack.nl/~marcov/compiler.pdf"></cta-button>

</br>

<cta-button text="More Books On Compiler Design " link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#compiler-design"></cta-button>
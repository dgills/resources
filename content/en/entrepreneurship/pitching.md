---
title: Pitching
description: We're compiling a library of useful resources about pitching your idea as an entrepreneur!
position: 1000
category: Entrepreneurship
---

## Articles

### Writing Your Pitch Narrative

This article will help you learn how to write your pitch narrative that will attract your desired audience and prevent your pitch from turning into a snooze fest.

[![Writing Your Pitch Narrative Preview](https://underscore.vc/wp-content/uploads/2021/03/Pitch-Narrative-Template.png)](https://underscore.vc/startupsecrets/pitch-narrative-template/)

[Source : underscore.vc](https://underscore.vc/)

<cta-button  link="https://underscore.vc/startupsecrets/pitch-narrative-template/" text="Read Article"></cta-button>


### How to Give A Perfect Investor Pitch

Your pitch should be built on the reality of your business plan. It’s not about the slides you show or the words you use. It's about your team, your value proposition, and your business strategy.

[![How to Give a Perfect Investor Pitch Preview](https://i.imgur.com/qIWYbEj.png)](https://underscore.vc/startupsecrets/how-to-pitch-to-investors/)

[Source : underscore.vc](https://underscore.vc/)

<cta-button  link="https://underscore.vc/startupsecrets/how-to-pitch-to-investors/" text="Read Article"></cta-button>

### Stunning Presentations Made Together.

Pitch is uncompromisingly good presentation software, enabling modern teams to craft and distribute beautiful presentations more effectively

[![Stunning Presentations Made Together Preview](https://i.imgur.com/iMxiJyI.png)](https://pitch.com/)

[Source : pitch.com](https://pitch.com/)

<cta-button  link="https://pitch.com/" text="Read Article"></cta-button>



## Videos

### How to Create A Pitch Deck for Investors

In this video, Slidebean's Kaya dig deep into the standard pitch deck outline most companies are using these days, and they give you some guidance as to what information and in what format should be included on your document.

<youtube-video id="SB16xgtFmco"> </youtube-video>


### 8 Red Flags When Pitching Investors

In this video [Slidebean](https://slidebean.com/) has extensively covered what a pitch deck should have and what your financials should answer.

<youtube-video id="FnkTO5Ml34U"> </youtube-video>


### Pitch Deck Mistakes: Go-To-Market Strategy

Go-to-Market Strategy is a super important item investors really want to understand, yet most startups don’t really know what the term means. They struggle to describe their go-to-market strategy in a clear and focused manner. Usually, when startups are asked about their go-to-market strategy they’d answer something like: “we’re doing direct sales” or “we’ll do online sales.” That’s not a go-to-market strategy. Those are sales tactics.

<youtube-video id="o0ygMSNZrHY"> </youtube-video>


### Startup Secrets: Getting Behind The Perfect Invester Pitch  

<iframe allowtransparency="true" title="Wistia video player" allowFullscreen frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" src="https://fast.wistia.net/embed/iframe/zolk1m0ezd" width="100%" height="350px"></iframe>

---
title: Software Leadership
description: We're compiling a library of useful software leadership resources from around the Internet to help you on your journey!
position: 1001
category: Entrepreneurship
---

## Refactoring by Luca Rossi

Refactoring is a weekly column about making great software, working with people, and personal growth.

[![Browsers Preview](https://i.imgur.com/ZyqyHZg.jpg)](https://refactoring.fm/archive?sort=top)

[Author : Luca Rossi](https://substack.com/profile/6835984-luca-rossi)

<cta-button  link="https://refactoring.fm/archive?sort=top" text="View Articles" > </cta-button>

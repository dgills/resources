---
title: Cheat Sheets
description: We're compiling a library of useful networking cheat sheets from around the Internet to help you on your software journey!
position: 17002
category: Networking
---

## TCP/IP Model Cheatsheet

[![TCP/IP Model Cheatsheet Preview](https://i.imgur.com/EilCurj.png)](https://networkwalks.com/tcpip-model-study-notes-cheatsheet/)

[Source: networkwalks.com](https://networkwalks.com/)

<cta-button text="View" link="https://networkwalks.com/tcpip-model-study-notes-cheatsheet/"></cta-button>


## Multiprotocol Label Switching

[![Multiprotocol Label Switching Preview](https://i.imgur.com/gDlLNiO.png)](https://ipwithease.com/mpls-cheatsheet/)

[Source: ipwithease.com](https://ipwithease.com/)

<cta-button text="View" link="https://ipwithease.com/mpls-cheatsheet/"></cta-button>


## TCP/UDP Port Numbers

[![TCP/UDP Port Numbers Preview](https://i.imgur.com/3rN1vGV.png)](https://www.stationx.net/common-ports-cheat-sheet/)

[Source: stationx.net](https://www.stationx.net/)

<cta-button text="View" link="https://www.stationx.net/common-ports-cheat-sheet/"></cta-button>


## Enhanced Interior Gateway Routing Protocol (EIGRP) 

[![TCP/UDP Port Numbers Preview](https://i.imgur.com/qik52wd.png)](https://ipwithease.com/eigrp-cheatsheet/)

[Source: ipwithease.com](https://ipwithease.com/)

<cta-button text="View" link="https://ipwithease.com/eigrp-cheatsheet/"></cta-button>


## IP Addressing Cheat Sheet

[![TCP/UDP Port Numbers Preview](https://i.imgur.com/ItaiQop.png)](https://cheatography.com/adeason/cheat-sheets/ip-addressing/)

[Source: cheatography.com](https://cheatography.com/)

<cta-button text="View" link="https://cheatography.com/adeason/cheat-sheets/ip-addressing/"></cta-button>


## Basic Cisco IOS Commands Cheat Sheet

[![TCP/UDP Port Numbers Preview](https://i.imgur.com/Nb9s3SY.png)](https://cheatography.com/tamaranth/cheat-sheets/basic-cisco-ios-commands/)

[Source: cheatography.com](https://cheatography.com/)

<cta-button text="View" link="https://cheatography.com/tamaranth/cheat-sheets/basic-cisco-ios-commands/"></cta-button>


## Networks - Physical Layer Cheat Sheet

[![TCP/UDP Port Numbers Preview](https://i.imgur.com/Rnd91dU.png)](https://cheatography.com/bayan-a/cheat-sheets/networks-physical-layer/)

[Source: cheatography.com](https://cheatography.com/)

<cta-button text="View" link="https://cheatography.com/bayan-a/cheat-sheets/networks-physical-layer/"></cta-button>


## Wireless Networks Cheat Sheet

[![TCP/UDP Port Numbers Preview](https://i.imgur.com/h9goSVK.png)](https://cheatography.com/phamine/cheat-sheets/wireless-networks/)

[Source: cheatography.com](https://cheatography.com/)

<cta-button text="View" link="https://cheatography.com/phamine/cheat-sheets/wireless-networks/"></cta-button>


---
title: Courses 
description: We're compiling a library of useful networking courses from around the Internet to help you on your software journey!
position: 17003
category: Networking
---

## Networking Essentials

Learn basic networking concepts and skills you can put to use right away, no previous networking knowledge needed.

[![Networking Essentials Preview ](https://i.imgur.com/b3BmyJt.png)](https://www.netacad.com/courses/networking/networking-essentials)

[Source : Cisco Networking Academy](https://www.netacad.com/)

<cta-button text="View" link="https://www.netacad.com/courses/networking/networking-essentials"></cta-button>
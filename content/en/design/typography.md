---
title: Typography
description: We're compiling useful resources around the internet to help you learn typography.
position: 1104
category: Design
---

## Typography 

Typography design is the art of arranging a message in a readable and aesthetically pleasing composition. 

<cta-button text="Learn More" link="https://99designs.com/blog/tips/typography-design/"></cta-button>

## Typography Critique

Learn typography with Chris Do as he reviews user-submitted work from his class and goes over the fundamentals of good typography. 

<youtube-video id="4h4RNmemzB0?list=PLroLjS4HDi0BmcTki-J7ax5M0GapbLQrW"> </youtube-video>

## How To Improve Your Layout and Typography Critique

This video will help you improve your graphic design skills and learn how to use type. Typography critique and review by Chris Do and the Futur team.

<youtube-video id="xjAQ5CK4oqg"> </youtube-video>

## Learn Typography Through This Poster Design Critique

Don't get stuck with your type layouts. Improve your typography skills by watching Chris Do & Emily Xie review and critique fan submitted design and layouts. They offer up helpful suggestions, design alternatives and explore different design options.

<youtube-video id="VH8QT73Gxwo"> </youtube-video>

<youtube-video id="0fvEFIkT7pM"> </youtube-video>


## 10 alternatives for Helvetica in 2022 : 
[![Helvetica Alternatives 2022](https://i.imgur.com/gVvuRa6.jpg)](https://resources.grey.software/design/typography/)

1. Open Sans(Free)
![](https://i.imgur.com/TMkhIrh.png)
<cta-button text="Try Open Sans" link="https://fonts.google.com/specimen/Open+Sans"></cta-button>

2. Inter(Free)
![](https://i.imgur.com/EtpGn7c.png)
<cta-button text="Try Inter" link="https://fonts.google.com/specimen/Inter"></cta-button>

3. Work Sans(Free) 
![](https://i.imgur.com/lczsY80.png)
<cta-button text="Try Work Sans" link="https://fonts.google.com/specimen/Work+Sans"></cta-button>

4. Stag Sans
![](https://i.imgur.com/KqJXLAQ.png)
<cta-button text="Try Stag Sans" link="https://commercialtype.com/catalog/stag/stag_sans"></cta-button>

5. Akzidenz-Grotesk
![](https://i.imgur.com/miLnCLx.png)
<cta-button text="Try Akzidenz Grotesk" link="https://www.bertholdtypes.com/font/akzidenz-grotesk/proplus/"></cta-button>

6. Avenir Next
![](https://i.imgur.com/NqlEDRo.png)
<cta-button text="Try Avenir" link="https://www.linotype.com/2090/avenir-next.html"></cta-button>

7. Arimo(Free)
![](https://i.imgur.com/PVeztku.png)
<cta-button text="Try Arimo" link="https://fonts.google.com/specimen/Arimo"></cta-button>

8. Univers
![](https://i.imgur.com/JnQGbWd.png)
<cta-button text="Try Univers" link="https://www.fontshop.com/families/univers"></cta-button>

9. Proxima Nova
![](https://i.imgur.com/9Yf3tQe.png)
<cta-button text="Try Proxima Nova" link="https://www.linotype.com/1130554/proxima-nova-family.html"></cta-button>

10. FF Bau

![](https://i.imgur.com/8jJAfpT.png)
<cta-button text="Try FF Bau" link="https://www.linotype.com/526670/ff-bau-family.html"></cta-button>



## Type Foundaries for your next Typeface purchase:
| Type Foundary  |
|----------|
| [ABCDinamo]( https://www.abcdinamo.com) |
| [Atipo foundry](http://atipofoundry.com) |
| [Berthold Types](https://www.bertholdtypes.com) |
| [Black Foundry](https://black-foundry.com)  |
| [Bold Monday](https://www.boldmonday.com) |
| [Bonjour Monde](http://bonjourmonde.net) |
| [BrightHead Studio](https://brightheadstudio.com) |
| [CNAP](https://www.cnap.fr) |
| [Colophon Foundry](https://www.colophon-foundry.org) |
| [Craft Supply Co.](https://www.behance.net/craftsupplyco ) |
| [Creative Tacos](https://creativetacos.com ) |
| [Dalton Maag](https://www.daltonmaag.com) |
| [Delve Fonts](https://delvefonts.com ) |
| [Dharma Type](https://dharmatype.com) |
| [Displaay Type Foundry](https://displaay.net) |
| [Etcetera Type Company](https://www.etc.supply) |
| [FaceType](https://www.facetype.org) | 
| [Family Type](https://familytype.com) |
| [Fontfabric](https://www.fontfabric.com) |
| [Fonts with Love](http://www.fontswithlove.com ) |
| [Gradient](https://wearegradient.net/ ) |
| [G-Type](https://g-type.com/) |
| [Hanken Design Co.](https://hanken.co) | 
| [Harvatt House](https://harvatt.house) |
| [Hoefler & Co.](https://www.typography.com ) | 
| [Indestructible Type](https://indestructibletype.com/Home.html) |  
| [Indian Type Foundry](https://www.indiantypefoundry.com) |  
| [JAM Type](https://jamtype.com) | 
| [Klim Type Foundry](https://klim.co.nz) | 
| [Lineto](https://lineto.com/) |  
| [Lino type](https://www.linotype.com/fr/) | 
| [Love Letters](http://love-letters.be) | 
| [MKCL](https://mckltype.com) |
| [Maous](http://maous.fr) |
| [Marc Simonson](https://www.marksimonson.com/) |
| [Microsoft](https://github.com/microsoft) | 
| [Milieu Grotesque](https://www.milieugrotesque.com ) |
| [Monotype](https://www.monotype.com/) | 
| [Nonpareille](https://www.nonpareille.net) |  
| [Pampatype](https://pampatype.com) | 
| [Pangram Pangram](https://pangrampangram.com) | 
| [Parachute](https://www.parachutefonts.com) | 
| [Production Type](https://www.productiontype.com) |  
| [Razzia Type](https://www.razziatype.com) |  
| [Schick-Toikka](https://www.schick-toikka.com) | 
| [Source Foundry](https://github.com/source-foundry) |  
| [Steppot](https://steppot.com) |
| [Suisse Typefaces](https://www.swisstypefaces.com) |
| [The League of Moveable Type](https://www.theleagueofmoveabletype.com) |  
| [Think Work Observ](https://t-wo.it) |  
| [Tribby Type](https://tribby.com) |  
| [TypeType](https://typetype.org) |  
| [Typefaces of The Temporary State](http://typefaces.temporarystate.net/) |  
| [Typotheque](https://www.typotheque.com) |  
| [URW Type Foundry](https://www.urwtype.com/en/) |  
| [Velvetyne](http://velvetyne.fr) | 
| [Wordshape](http://wordshape.com) | 
| [Zeta Fonts](https://www.zetafonts.com) |  


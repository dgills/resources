---
title: Fonts
description: We're compiling useful fonts' resources around the internet to help you learn digital design.
position: 1123
category: Design
---

## Best Font finders

### Font Finder 

FontFinder is google chrome extension created for designers, developers, and typographers. It allows a user to analyze the font information of any element and copy any pieces of that information to the clipboard. Font Finder examines all the selected characters to detect the font used to display this particular character.

[![Font Finder  Preview](https://i.imgur.com/jbCj839.png)](https://chrome.google.com/webstore/detail/font-finder/bhiichidigehdgphoambhjbekalahgha)

[Source : Chrome Web Store](https://chrome.google.com/webstore/category/extensions)

<cta-button text="Learn More" link="https://chrome.google.com/webstore/detail/font-finder/bhiichidigehdgphoambhjbekalahgha"></cta-button>

 
### WhatFont

This google chrome extension is the easiest way to identify fonts on web pages. You could inspect web fonts by just hovering on them. It is that simple and elegant.

[![WhatFont Preview](https://i.imgur.com/vBn6kh8.png)](https://chrome.google.com/webstore/detail/whatfont/jabopobgcpjmedljpbcaablpmlmfcogm)

[Source : Chrome Web Store](https://chrome.google.com/webstore/category/extensions)

<cta-button text="Learn More" link="https://chrome.google.com/webstore/detail/whatfont/jabopobgcpjmedljpbcaablpmlmfcogm"></cta-button>


## Ultimate Guide To Font Sizes In UI Design

This guide has up-to-date  guidelines and best practices for font sizes across all major platforms – iOS 15, Android/Material Design, and responsive web.

[![Ultimate Guide To Font Sizes In UI Design Preview](https://i.imgur.com/82Jy2Sk.png)](https://www.learnui.design/blog/ultimate-guide-font-sizes-ui-design.html)

[Source : learnui.design/blog](https://www.learnui.design/blog)

<cta-button text="Learn More" link="https://www.learnui.design/blog/ultimate-guide-font-sizes-ui-design.html"></cta-button>


## 15 Google Fonts for your Next Design Project in 2022

| Fonts &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;|
| ----------------------- |
| [DM Sans : San serif](https://fonts.google.com/specimen/DM+Sans)  | 
| [Space Grotesk : San serif](https://fonts.google.com/specimen/Space+Grotesk) |
| [Inter : San serif](https://fonts.google.com/specimen/Inter) |
| [Work Sans : San serif](https://fonts.google.com/specimen/Work+Sans) |
| [Manrope : San serif](https://fonts.google.com/specimen/Manrope) |
| [Fira : San serif](https://fonts.google.com/specimen/Fira+Sans) |
| [PT Serif : Serif](https://fonts.google.com/specimen/PT+Serif) |
| [Cardo : Serif](https://fonts.google.com/specimen/Cardo) |
| [Libre Franklin : San serif](https://fonts.google.com/specimen/Libre+Franklin) |
| [Lora : Serif](https://fonts.google.com/specimen/Lora) |
| [Playfair Display : Serif](https://fonts.google.com/specimen/Playfair+Display) |
| [Syne : San serif](https://fonts.google.com/specimen/Syne) |
| [Libre Baskerville : Serif](https://fonts.google.com/specimen/Libre+Baskerville) |
| [Alegreya : Serif](https://fonts.google.com/specimen/Alegreya) |
| [Anek : San serif](https://befonts.com/anek-font-family.html) |

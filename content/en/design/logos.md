---
title: Logo
description: We're compiling useful resources around the internet to help you learn logo design.
position: 1102
category: Design 
---

## What Makes A Logo Great & Iconic?

What is a good logo? What is a bad logo? A logo is not communication. It's identification. It's the period at the end of sentence and not the sentence. How do you create a simple but not generic logo? Should a logo be complicated? How can you justify the price of a simple logo? Is a simple logo easy or hard to create? How can you make a timeless logo?

Logos are never love at first sight. 
It takes time to develop. 
A trademark gains meaning and power over time.

<youtube-video id="TH-jqZ4xueI"> </youtube-video>


## Logo Design

Learn how to design beautiful and noteworthy logos. It has all the video content, course material, and tools to help you perfect your logo designs and process

[![Logo Design Preview ](https://i.imgur.com/uB3T4Rx.png)](https://thefutur.com/tags/logo-design)

[Source : thefutur.com](https://thefutur.com/)

<cta-button text="Learn More" link="https://thefutur.com/tags/logo-design"></cta-button>


## Logobook

Discover the worlds finest logos, symbols & trademarks.

[![Logobook Preview ](https://i.imgur.com/MNRi5Z7.png)](http://www.logobook.com/)

[Source : logobook.com](http://www.logobook.com/)

<cta-button text="Learn More" link="http://www.logobook.com/"></cta-button>



| Website&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; | Description |
| ----------------------- | ------------------ |
| [Instant Logo Search](http://instantlogosearch.com/)| Thousands of free brands logos ( SVG - PNG ) |
| [LogoSear.ch](https://logosear.ch/search.html) | Search engine with over 200,000 SVG logos indexed |
| [Browser Logos](https://github.com/alrra/browser-logos/) | High resolution web browser logos |
| [VectorLogoZone](https://www.vectorlogo.zone/) | Consistently formatted SVG logos |
| [World Vector Logo](https://worldvectorlogo.com/)| Download vector logos of brands you love |
| [Logo Maker](https://logomakr.com/)| Create custom logos |
| [Namecheap Logo Maker](https://www.namecheap.com/logo-maker/)| Create and download custom Logos for free |




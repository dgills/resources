---
title: Open Education
description: We're compiling a library of useful Open Source assets from around the Internet to help you on your software journey!
position: 17501
category: Open Source 
---

## Rethink Education

Find free online courses, lectures, and videos from the best colleges in the country. Take online classes from schools like Yale, MIT and Stanford.

[![Rethink Education Preview ](https://i.imgur.com/qUZSQwJ.png)](https://academicearth.org/)

[Source : academicearth.org](https://academicearth.org/)

<cta-button text="View" link="https://academicearth.org/"></cta-button>


## Best Science Website for Teachers

This website is easily accessible to any science student/teacher where they can download freely available, high quality, locally adaptable full-course materials. Because all of the materials are open-source, also known as open educational resources, they are free for all educators and students to use, customize, and share. 

[![Best Science Website for Teachers Preview ](https://i.imgur.com/1PkaiH3.png)](https://www.openscied.org/access-the-materials/)

[Source : openscied.org](https://www.openscied.org/)

<cta-button text="View" link="https://www.openscied.org/access-the-materials/"></cta-button>


## Explore. Create. Collaborate

It is a public digital library of open educational resources. Explore, create, and collaborate with educators around the world to improve curriculum.

[![Explore. Create. Collaborate Preview ](https://i.imgur.com/dIyAyoC.png)](https://www.oercommons.org/)

[Source : www.oercommons.org](https://www.oercommons.org/)

<cta-button text="View" link="https://www.oercommons.org/"></cta-button>


## Online Education Leader

Get the latest in online education. Find online courses and compare the top-ranked online bachelor's, master's, associate, and certificate programs.

[![Online Education Leader Preview ](https://i.imgur.com/UrzBd6X.png)](https://oedb.org/)

[Source : oedb.org](https://oedb.org/)

<cta-button text="View" link="https://oedb.org/"></cta-button>


## Open Source At DataCamp

DataCamp aims to give back to the open source community by open sourcing parts of their platform and stack.

[![Online Education Leader Preview ](https://i.imgur.com/cIQkLH0.png)](https://www.datacamp.com/open-source)

[Source : datacamp.com](https://www.datacamp.com/)

<cta-button text="View" link="https://www.datacamp.com/open-source"></cta-button>



---
title: Cheat Sheets
description: We're curating cheatsheets from around the internet to help you on your software journey!
category: Overview
position: 5
---

## Interactive HTML Cheat Sheet

An HTML Cheat Sheet that contains the most common style snippets and an interactive HTML-CSS editor on the bottom of the page that gives you a live preview to test and adjust the code further.

[![Interactive HTML CheatSheet Preview](https://htmlcheatsheet.com/images/html-cheatsheet.jpg)](https://htmlcheatsheet.com)

<cta-button text="View" link="https://htmlcheatsheet.com"></cta-button>

## Interactive CSS Cheat Sheet

A CSS Cheat Sheet that contains the most common style snippets and an interactive HTML-CSS editor on the bottom of the page that gives you a live preview to test and adjust the code further.

[![Interactive CSS CheatSheet Preview](https://i.imgur.com/IXTeBYy.png)](https://htmlcheatsheet.com/css)

<cta-button text="View" link="https://htmlcheatsheet.com/css"></cta-button>

## CSS Cheat Sheet by Dev Hints

Dev Hints is a collection of cheatsheets written by [@rstacruz](https://ricostacruz.com/).

[![Grid Garden Preview](https://assets.devhints.io/previews/css.jpg?t=20210620130816)](https://devhints.io/css)

<cta-button text="View" link="https://devhints.io/css"></cta-button>

## HTML & CSS Emmet Cheat Sheet

Emmet is a web-developer’s toolkit that can greatly improve your HTML & CSS workflow!

<a href="https://docs.emmet.io/cheat-sheet/"> <img src="http://emmet.io/i/logo-large.png" alt="Emmet Logo"
style="object-fit:cover;height:343px;width:100%;"></a>

<cta-button text="View" link="https://docs.emmet.io/cheat-sheet/"></cta-button>

- [JavaScript Cheatsheet - Codecademy (HTML)](https://www.codecademy.com/learn/introduction-to-javascript/modules/learn-javascript-introduction/cheatsheet)

- [JavaScript CheatSheet (HTML)](https://htmlcheatsheet.com/js/)

- [Nuxt.js Essentials Cheatsheet - Vue Mastery (PDF)](https://www.vuemastery.com/pdf/Nuxtjs-Cheat-Sheet.pdf)

- [React Cheatsheet - Codecademy (HTML)](https://www.codecademy.com/learn/react-101/modules/react-101-jsx-u/cheatsheet)

- [Vue Essential Cheatsheet - Vue Mastery (PDF)](https://www.vuemastery.com/pdf/Vue-Essentials-Cheat-Sheet.pdf)

- [Vue.js 2.3 Complete API Cheat Sheet - Marcos Neves, Marozed (HTML)](https://marozed.com/vue-cheatsheet)

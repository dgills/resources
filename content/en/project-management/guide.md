---
title: Guides
description: We're compiling a library of useful project management guides from around the Internet to help you on your software journey!
position: 19000
category: Project Management 
---

## Project Management Kit

A detailed guide that will help you tackle project management tasks with free resources and tools.

[![Project Management Kit Preview](https://i.imgur.com/qhDytsu.png)](https://nonchalant-slip-f67.notion.site/Project-Management-Kit-be0e67c89f2d4c5db8e2d60638c4a670)

[Source : formeer.io](https://formeer.io/)

<cta-button text="View" link="https://nonchalant-slip-f67.notion.site/Project-Management-Kit-be0e67c89f2d4c5db8e2d60638c4a670"></cta-button>
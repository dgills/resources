---
title: Guides
description: We're compiling useful Git guides around the internet to help you on your software journey!
position: 1500
category: Git
---

## Git

### Introduction to Git and Github 

<book-cover link="http://cse.unl.edu/~cbourke/gitTutorial.pdf" img-src="https://i.imgur.com/UNvnARX.png" alt="Book cover for Introduction to Git and Github  "> </book-cover>

This tutorial will describe how to use Github for use in your courses and to manage and share your code among your peers for group assignments and projects

<cta-button text="Complete Guide" link="http://cse.unl.edu/~cbourke/gitTutorial.pdf"></cta-button>


### Git - The Simple Guide

<book-cover link="https://rogerdudler.github.io/git-guide/" img-src="https://i.imgur.com/hEIchOm.png" alt="Book cover for Official Intro Tutorial"> </book-cover>

A simple guide for getting started with Git. 

<cta-button text="Complete Guide" link="https://rogerdudler.github.io/git-guide/"></cta-button>


### Learn Git 

<book-cover link="https://www.git-tower.com/learn/git/ebook" img-src="https://i.imgur.com/rwkJfq9.png" alt="Book cover for Learn Git"> </book-cover>

Version control is an essential tool if you want to be successful in today's web & software world. This online book will help you master it with ease.

<cta-button text="Complete Guide" link="https://www.git-tower.com/learn/git/ebook"></cta-button>


### Pro Git

<book-cover link="https://git-scm.com/book/en/v2" img-src="https://i.imgur.com/8RxPd60.png" alt="Book cover for Pro Git"> </book-cover>

Effective and well-implemented version control is a necessity for successful web projects, whether large or small. 

With this book you’ll learn how to master the world of distributed version workflow, use the distributed features of Git to the full, and extend Git to meet your every need. 

<cta-button text="Complete Guide" link="https://git-scm.com/book/en/v2"></cta-button> <cta-button text="Download PDF" link="https://github.com/progit/progit2/releases/download/2.1.339/progit.pdf"></cta-button>

## Git Tutorial - W3Schools

<book-cover link="https://www.w3schools.com/git/" img-src="https://i.imgur.com/pdxAH5A.png" alt="Book cover for Git Tutorial - W3Schools Tutorial"> </book-cover>

Git is a popular version control system. It was created by Linus Torvalds in 2005, and has been maintained by Junio Hamano since then.

It is used for:

- Tracking code changes
- Tracking who made changes
- Coding collaboration

<cta-button text="Full Tutorial" link="https://www.w3schools.com/git/"></cta-button>

</br>

<cta-button text="More Guides On Git" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#git"></cta-button>




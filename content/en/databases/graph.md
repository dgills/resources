---
title: Graph Databases
description: We're compiling a library of useful graph database resources from around the Internet to help you on your software journey!
position: 5001
category: Databases
---

## What is a Graph Database?

In computing, a graph database (GDB) is a database that uses graph structures for semantic queries with nodes, edges, and properties to represent and store data. 

<youtube-video id="REVkXVxvMQE"> </youtube-video>

## GUN Decentralized Graph Database

GUN is an open-source decentralized database service that allows developers to build fast peer-to-peer applications that will work, even when their users are offline

This video helps you learn how to use the GUN JavaScript API and start writing data to your own peer-to-peer network. 

<youtube-video id="oTQXzhm8w_8"> </youtube-video>

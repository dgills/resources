---
title: Books
description: We're compiling a library of useful database books from around the Internet to help you on your software journey!
position: 5002
category: Databases
---

## Database

### Database Explorations

<book-cover link="https://www.dcs.warwick.ac.uk/~hugh/TTM/Database-Explorations-revision-2.pdf" img-src="https://i.imgur.com/fZyovzS.jpg" alt="Book cover for Database Explorations"> </book-cover>

The database field is full of important problems still to be solved and interesting issues that need to be examined. Some of those problems and issues are explored in this book. It reports on some of the most recent investigations in this field.

<cta-button text="Download PDF" link="https://www.dcs.warwick.ac.uk/~hugh/TTM/Database-Explorations-revision-2.pdf"></cta-button>

### Database Fundamentals 

<book-cover link="http://public.dhe.ibm.com/software/dw/db2/express-c/wiki/Database_fundamentals.pdf" img-src="https://i.imgur.com/MkCTK4m.png" alt="Book cover for Database Fundamentals "> </book-cover>

Learn the basics of relational database theory and other information models. This book discusses database logical and physical design, and introduces you to the SQL language. Practice with hands-on exercises.

<cta-button text="Download PDF" link="http://public.dhe.ibm.com/software/dw/db2/express-c/wiki/Database_fundamentals.pdf"></cta-button>

### Databases, Types, and The Relational Model: The Third Manifesto 

<book-cover link="https://www.dcs.warwick.ac.uk/~hugh/TTM/DTATRM.pdf" img-src="https://i.imgur.com/OSdsc3x.jpg" alt="Book cover for Databases, Types, and The Relational Model: The Third Manifesto "> </book-cover>

This is a book on database management that is based on an earlier book by the same authors,Foundation for Future Database Systems: The Third Manifesto. It can be seen as an abstract blueprint for the design of a DBMS and the language interface to such a DBMS. In particular, it serves as a basis for a model of type inheritance. This book is essential reading for database professionals.

<cta-button text="Download PDF" link="https://www.dcs.warwick.ac.uk/~hugh/TTM/DTATRM.pdf"></cta-button>

</br>

<cta-button text="More Resources On Database" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#database"></cta-button>


## MongoDB

### Introduction to MongoDB

<book-cover link="https://www.tutorialspoint.com/mongodb/" img-src="https://i.imgur.com/Wk9MDif.jpg" alt="Book cover for Introduction to MongoDB"> </book-cover>

This interactive tutorial is designed for Software Professionals who are willing to learn MongoDB Database in simple and easy steps. It will throw light on MongoDB concepts and after completing this tutorial you will be at an intermediate level of expertise, from where you can take yourself at higher level of expertise.

<cta-button text="Complete Book" link="https://www.tutorialspoint.com/mongodb/"></cta-button>

</br>

<cta-button text="More Books On MongoDB" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#mongodb"></cta-button>


## SQL

### Developing Time-Oriented Database Applications in SQL 

<book-cover link="https://www2.cs.arizona.edu/~rts/tdbbook.pdf" img-src="https://i.imgur.com/rGqOrHQ.jpg" alt="Book cover for Developing Time-Oriented Database Applications in SQL"> </book-cover>

Whether you are a database designer, programmer, analyst, or manager, you have probably encountered some challenges and experienced some of the frustrations associated with time-varying data. Where do you turn to fix the problem and see that it does not happen again?In Developing Time-Oriented Database Applications in SQL, a leading SQL researcher teaches you effective techniques for designing and building database applications that must integrate past and current data.

Written to meet a pervasive, enduring need, this book will be indispensable if you happen to be part of the flurry of activity leading up to Y2K.

<cta-button text="Download PDF" link="https://www2.cs.arizona.edu/~rts/tdbbook.pdf"></cta-button>


### Introduction to SQL 

<book-cover link="https://github.com/bobbyiliev/introduction-to-sql" img-src="https://i.imgur.com/wOfvsjW.png" alt="Book cover for Introduction to SQL "> </book-cover>

This is an open-source introduction to SQL guide that will help you to learn the basics of SQL and start using relational databases for your SysOps, DevOps, and Dev projects. No matter if you are a DevOps/SysOps engineer, developer, or just a Linux enthusiast, you will most likely have to use SQL at some point in your career.

The guide is suitable for anyone working as a developer, system administrator, or a DevOps engineer and wants to learn the basics of SQL.

<cta-button text="Download PDF" link="https://github.com/bobbyiliev/introduction-to-sql"></cta-button>


### SQL For Web Nerds

<book-cover link="http://philip.greenspun.com/sql/" img-src="https://i.imgur.com/9aXhxeF.jpg" alt="Book cover for SQL For Web Nerds"> </book-cover>

This is a free online SQL book by Philip Greenspun. It covers data modeling, simple queries, complex queries, transactions, triggers, views, style, PL/SQL and Java, trees, handling dates in Oracle, limits in Oracle, tuning, data warehousing, foreign and legacy data and normalization.

<cta-button text="Complete Book" link="http://philip.greenspun.com/sql/"></cta-button>


</br>

<cta-button text="More Books On SQL" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#sql-implementation-agnostic"></cta-button>

<cta-button text="More Resources On SQL" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#sql"></cta-button>


## SQL Server

### Defensive Database Programming 

<book-cover link="https://www.red-gate.com/library/defensive-database-programming" img-src="https://i.imgur.com/PGhLZB7.png" alt="Book cover for Defensive Database Programming "> </book-cover>

The goal of Defensive Programming is to produce resilient code that responds gracefully to the unexpected. To the SQL Server programmer, this means T-SQL code that behaves consistently and predictably in cases of unexpected usage, doesn't break under concurrent loads, and survives predictable changes to database schemas and settings.

Inside this book, you will find dozens of practical, defensive programming techniques that will improve the quality of your T-SQL code and increase its resilience and robustness.

<cta-button text="Download PDF" link="https://www.red-gate.com/library/defensive-database-programming"></cta-button>


### Mastering SQL Server Profiler 

<book-cover link="https://www.red-gate.com/library/mastering-sql-server-profiler" img-src="https://i.imgur.com/6bdgS1w.png" alt="Book cover for "> </book-cover>

SQL Server Profiler is one of the most useful of SQL Server's "built-in" tools, recording data about various SQL Server events. "Mastering SQL Server Profiler" will make it easier for you to learn how to use Profiler, analyze the data it provides, and to take full advantage of its potential for troubleshooting SQL Server problems. All the examples have been optimized to work with both SQL Server 2005 and 2008.

<cta-button text="Download PDF" link="https://www.red-gate.com/library/mastering-sql-server-profiler"></cta-button>


### Protecting SQL Server Data 

<book-cover link="https://www.red-gate.com/library/protecting-sql-server-data" img-src="https://i.imgur.com/Yci7rdk.jpg" alt="Book cover for Protecting SQL Server Data "> </book-cover>

In Protecting SQL Server Data, John Magnabosco demonstrates how sensitive data, stored in SQL Server, can be protected using an efficient and maintainable encryption-based data architecture.

He explains how to assess and categorize data elements according to sensitivity, regulate access to the various categories of data using database roles, views and stored procedures, and then how to implement a secure data architecture using features such as cell-level encryption, transparent data encryption, one-way encryption, obfuscation, and more.

<cta-button text="Download PDF" link="https://www.red-gate.com/library/protecting-sql-server-data"></cta-button>

</br>

<cta-button text="More Books On SQL Server" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#sql-server"></cta-button>


## MySQL

### MySQL 8.0 Tutorial Excerpt 

<book-cover link="https://downloads.mysql.com/docs/mysql-tutorial-excerpt-8.0-en.pdf" img-src="https://i.imgur.com/Lp0cjlR.jpg" alt="Book cover for MySQL 8.0 Tutorial Excerpt"> </book-cover>

This is the MySQL Tutorial from the MySQL 8.0 Reference Manual.

<cta-button text="Download PDF" link="https://downloads.mysql.com/docs/mysql-tutorial-excerpt-8.0-en.pdf"></cta-button>

</br>

<cta-button text="More Books On MySQL" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#mysql"></cta-button>


## NoSQL

### NoSQL Databases 

<book-cover link="https://www.christof-strauch.de/nosqldbs.pdf" img-src="https://i.imgur.com/HnPjoPZ.jpg" alt="Book cover for NoSQL Databases "> </book-cover>

A NoSQL database environment is, simply put, a non-relational and largely distributed database system that enables rapid, ad-hoc organization and analysis of extremely high-volume, disparate data types.

NoSQL databases are sometimes referred to as cloud databases, non-relational databases, Big Data databases and a myriad of other terms and were developed in response to the sheer volume of data being generated, stored and analyzed by modern users (user-generated data) and their applications (machine-generated data).

<cta-button text="Download PDF" link="https://www.christof-strauch.de/nosqldbs.pdf"></cta-button>

### The Little Redis Book 

<book-cover link="https://www.openmymind.net/redis.pdf" img-src="https://i.imgur.com/zq5XsCO.png" alt="Book cover for The Little Redis Book"> </book-cover>

<cta-button text="Download PDF" link="https://www.openmymind.net/redis.pdf"></cta-button>

</br>

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#nosql"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#nosql"></cta-button>


## PostgreSQL

### The Internals of PostgreSQL for database administrators and system developers

<book-cover link="http://www.interdb.jp/pg/" img-src="https://i.imgur.com/HJv7Fta.png" alt="Book cover for The Internals of PostgreSQL for database administrators and system developers"> </book-cover>

In this document, the internals of PostgreSQL for database administrators and system developers are described.

PostgreSQL is an open source multi-purpose relational database system which is widely used throughout the world. It is one huge system with the integrated subsystems, each of which has a particular complex feature and works with each other cooperatively. Although understanding of the internal mechanism is crucial for both administration and integration using PostgreSQL, its hugeness and complexity prevent it. The main purposes of this document are to explain how each subsystem works, and to provide the whole picture of PostgreSQL.



<cta-button text="Complete Book" link="http://www.interdb.jp/pg/"></cta-button>

</br>

<cta-button text="More Books On PostgreSQL" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#postgresql"></cta-button>

<cta-button text="More Resources On PostgreSQL" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#postgresql"></cta-button>



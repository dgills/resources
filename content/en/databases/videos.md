---
title: Videos
description: We're compiling a library of useful databases from around the Internet to help you on your software journey!
position: 5000
category: Databases
---

## 7 Database Paradigms

This video will help you learn about seven different database paradigms and what they do best.

<youtube-video id="W2Z7fbCLSTw"> </youtube-video>

## SQL

SQL stands for Structured Query Language. It is used for storing and managing data in relational database management system (RDMS). It is a standard language for Relational Database System. It enables a user to create, read, update and delete relational databases and tables.

<youtube-video id="zsjvFFKOm3c"> </youtube-video>

## MySQL

MySQL is an Oracle-backed open source relational database management system (RDBMS) based on Structured Query Language (SQL). MySQL runs on virtually all platforms, including Linux, UNIX and Windows.

Here is a full tutorial for beginners on MySQL.

<youtube-video id="Cz3WcZLRaWc?list=PL0vfts4VzfNiq0-fXbVVdnngU1Ur2SzyZ"> </youtube-video>

## MongoDB

MongoDB is a document-oriented NoSQL database used for high volume data storage. Instead of using tables and rows as in the traditional relational databases, MongoDB makes use of collections and documents.

The document data model is a powerful way to store and retrieve data that allows developers to move fast.

<youtube-video id="-bt_y4Loofg?list=PL0vfts4VzfNiI1BsIK5u7LpPaIDKMJIDN"> </youtube-video>

## FaunaDB

FaunaDB is one of the most popular upcoming databases on the cloud. It provides amazing flexibility and scalability.

<youtube-video id="2CipVwISumA?list=PL0vfts4VzfNiq0-fXbVVdnngU1Ur2SzyZ"> </youtube-video>

---
title: Assets
description: We're compiling a library of useful assets from around the Internet to help you build technologies in artificial intelligence!
position: 5508
category: Artificial Intelligence
---


## There's an AI For That

Comprehensive database of AIs available for every task.

[![There's an AI For That Preview](https://i.imgur.com/MuoWJoA.png)](https://theresanaiforthat.com/)

[Source : theresanaiforthat.com](https://theresanaiforthat.com/)

<cta-button text="View" link="https://theresanaiforthat.com/"></cta-button>
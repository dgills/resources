---
title: Cheat Sheets
description: We're compiling a library of useful cheet sheats from around the Internet to help you build technologies in artificial intelligence!
position: 5507
category: Artificial Intelligence
---


## Tensorflow 

### TensorFlow Quick Reference Table

[![TensorFlow Quick Reference Table Preview](https://i.imgur.com/ZLW4Tzm.png)](https://secretdatascientist.com/tensor-flow-cheat-sheet/)

[Source : Secretdatascientist.com](https://secretdatascientist.com/)

<cta-button text="View" link="https://secretdatascientist.com/tensor-flow-cheat-sheet/"></cta-button>


### TensorFlow v2.0 Cheat Sheet

[![TensorFlow v2.0 Cheat Sheet Preview](https://i.imgur.com/PueJwJj.png)](https://web.archive.org/web/20200922212358/https://www.aicheatsheets.com/static/pdfs/tensorflow_v_2.0.pdf)

[Source : Altoros.com/visuals](https://www.altoros.com/visuals)

<cta-button text="View" link="https://web.archive.org/web/20200922212358/https://www.aicheatsheets.com/static/pdfs/tensorflow_v_2.0.pdf"></cta-button>

</br>

<cta-button text="More Cheatsheets On Tensorflow" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#tensorflow"></cta-button>


## PyTorch

### PyTorch Framework Cheat Sheet 

[![PyTorch Framework Cheat Sheet Preview](https://i.imgur.com/8D95yLB.png)](https://www.simonwenkel.com/publications/cheatsheets/pdf/cheatsheet_pytorch.pdf)

[Author : Simon Wenkel](https://www.simonwenkel.com/)

<cta-button text="View" link="https://www.simonwenkel.com/publications/cheatsheets/pdf/cheatsheet_pytorch.pdf"></cta-button>


### PyTorch Official Cheat Sheet 

[![PyTorch Official Cheat Sheet Preview](https://i.imgur.com/PtJI3he.png)](https://pytorch.org/tutorials/beginner/ptcheat.html)

[Source : Pytorch.org](https://pytorch.org/)

<cta-button text="View" link="https://pytorch.org/tutorials/beginner/ptcheat.html"></cta-button>

</br>

<cta-button text="More Cheatsheets On PyTorch" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#pytorch"></cta-button>


## Python

### Comprehensive Python Cheatsheet 

[![Comprehensive Python Cheatsheet Preview](https://i.imgur.com/8UJGlg6.png)](https://gto76.github.io/python-cheatsheet/)

[Author: Jure Šorn](https://gto76.github.io/)

<cta-button text="View" link="https://gto76.github.io/python-cheatsheet/"></cta-button>


### Learn Python in Y minutes 

[![Learn Python in Y minutes Preview](https://i.imgur.com/bITb8TT.png)](https://learnxinyminutes.com/docs/python/)

[Source: Learnxinyminutes.com](https://learnxinyminutes.com/)

<cta-button text="View" link="https://learnxinyminutes.com/docs/python/"></cta-button>


### Official Matplotlib cheat sheets 

[![Official Matplotlib cheat sheets](https://i.imgur.com/ACZL5qx.png)](https://github.com/matplotlib/cheatsheets)

[Source: Matplotlib Developers - Github](https://github.com/matplotlib)

<cta-button text="View" link="https://github.com/matplotlib/cheatsheets"></cta-button>


### Learn Python 3

[![ Learn Python 3](https://i.imgur.com/tTRiM6Z.png)](https://www.codecademy.com/learn/learn-python-3)

[Source: Codecademy.com](https://www.codecademy.com/)

<cta-button text="View" link="https://www.codecademy.com/learn/learn-python-3"></cta-button>


### Python Crash Course - Cheat Sheets

[![Python Crash Course - Cheat Sheets](https://i.imgur.com/wDqjHLl.png)](https://ehmatthes.github.io/pcc/cheatsheets/README.html)

[Author: Eric Matthes](https://ehmatthes.github.io/)

<cta-button text="View" link="https://ehmatthes.github.io/pcc/cheatsheets/README.html"></cta-button>


### Python Cheat Sheet

[![Python Cheat Sheet](https://i.imgur.com/zBBo00o.png)](https://cheatography.com/davechild/cheat-sheets/python/)

[Source: Cheatography.com](https://cheatography.com/)

<cta-button text="View" link="https://cheatography.com/davechild/cheat-sheets/python/"></cta-button>


### Python for Data Science Cheatsheets

[![Python Cheat Sheet](https://i.imgur.com/hEUK4cm.jpg)](https://pydatascience.org/data-science-cheatsheets/)

[Source: Pydatascience.org](https://pydatascience.org/)

<cta-button text="View" link="https://pydatascience.org/data-science-cheatsheets/"></cta-button>


</br>

<cta-button text="More Cheatsheets On Python" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#python"></cta-button>


## R

### All RStudio cheatsheets resources 

[![All RStudio cheatsheets resources Preview](https://i.imgur.com/D97iUyJ.png)](https://www.rstudio.com/resources/cheatsheets/)

[Source : Rstudio.com](https://www.rstudio.com/)

<cta-button text="View" link="https://www.rstudio.com/resources/cheatsheets/"></cta-button>

</br>

<cta-button text="More Cheatsheets On R" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#r"></cta-button>
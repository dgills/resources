---
title: Books
description:  We're compiling a library of useful programming books from around the Internet to help you on your software journey!
position: 12505
category: Programming
---



## Programming Paradigms

### Flow based Programming 

<book-cover link="https://jpaulm.github.io/fbp/index.html" img-src="https://i.imgur.com/U3lcPTi.png" alt="Book cover for Flow based Programming"> </book-cover>

This explains the theoretical underpinnings and application of flow based Programming method in practical terms. Readers are shown how to apply this programming in a number of areas and how to avoid common pitfalls.

[Author : J. Paul Rodker Morrison](https://jpaulm.github.io/index.html)

<cta-button text="Complete Book" link="https://jpaulm.github.io/fbp/index.html"></cta-button>


### Introduction to Functional Programming 

<book-cover link="https://www.cl.cam.ac.uk/teaching/Lectures/funprog-jrh-1996/" img-src="https://i.imgur.com/4tUek5A.png" alt="Book cover for Introduction to Functional Programming "> </book-cover>

This book serves as the lecture notes for the Introduction to Functional Programming course, at Cambridge University.

[Author : J. Harrison](https://www.cl.cam.ac.uk/~jrh13/)

<cta-button text="Complete Book" link="https://www.cl.cam.ac.uk/teaching/Lectures/funprog-jrh-1996/"></cta-button>


### Making Sense of Stream Processing 

<book-cover link="https://assets.confluent.io/m/2a60fabedb2dfbb1/original/20190307-EB-Making_Sense_of_Stream_Processing_Confluent.pdf" img-src="https://i.imgur.com/qFPgsAr.png" alt="Book cover for Making Sense of Stream Processing "> </book-cover>

This book shows you how stream processing can make your data storage and processing systems more flexible and less complex. Structuring data as a stream of events isn't new, but with the advent of open source projects such as Apache Kafka and Apache Samza, stream processing is finally coming of age.

[Author : Martin Kleppmann](https://martin.kleppmann.com/)

<cta-button text="Complete Book" link="https://assets.confluent.io/m/2a60fabedb2dfbb1/original/20190307-EB-Making_Sense_of_Stream_Processing_Confluent.pdf"></cta-button>

</br>

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#programming-paradigms"></cta-button>


## Parallel Programming

### Programming on Parallel Machines; GPU, Multicore, Clusters and More 

<book-cover link="https://heather.cs.ucdavis.edu/parprocbook" img-src="https://i.imgur.com/9v8gRWC.png" alt="Book cover for Programming on Parallel Machines; GPU, Multicore, Clusters and More"> </book-cover>

The main goal of this book is to present parallel programming techniques that can be used in many situations for many application areas and which enable the reader to develop correct and efficient parallel programs. Many examples and exercises are provided to show how to apply the techniques

[Author : Norm Matloff Kerridge](https://heather.cs.ucdavis.edu/matloff.html)

<cta-button text="Complete Book" link="https://heather.cs.ucdavis.edu/parprocbook"></cta-button>


### Is Parallel Programming Hard, And, If So, What Can You Do About It? 

<book-cover link="https://mirrors.edge.kernel.org/pub/linux/kernel/people/paulmck/perfbook/perfbook.html" img-src="https://i.imgur.com/LEz8ERw.jpg" alt=" Book cover for Is Parallel Programming Hard, And, If So, What Can You Do About It? "> </book-cover>

This book examines what makes parallel programming hard, and describes design techniques that can help you avoid many parallel-programming pitfalls. It is primarily intended for low-level C/C++ code, but offers valuable lessons for other environments as well.

[Author : Paul E. McKenney](http://www2.rdrop.com/~paulmck/)

<cta-button text="Complete Book" link="https://mirrors.edge.kernel.org/pub/linux/kernel/people/paulmck/perfbook/perfbook.html"></cta-button>


### High Performance Computing 

<book-cover link="https://cnx.org/contents/bb821554-7f76-44b1-89e7-8a2a759d1347%405.2" img-src="https://i.imgur.com/5G141CW.jpg" alt="Book cover for High Performance Computing "> </book-cover>

This book discusses how modern workstations get their performance and how you can write code that makes optimal use of your hardware. You'll learn what the newest buzzwords really mean, how caching and other memory design features affect the way your software behaves, and where the newest "post-RISC" architectures are headed.

[Authors : Charles Severance](https://www.dr-chuck.com/) [& Kevin Dowd](https://www.kevindowd.org/)

<cta-button text="Complete Book" link="https://cnx.org/contents/bb821554-7f76-44b1-89e7-8a2a759d1347%405.2"></cta-button>

</br>

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#programming-paradigms"></cta-button>


## Competitive Programming


### Competitive Programmer's Handbook

<book-cover link="https://cses.fi/book/book.pdf" img-src="https://i.imgur.com/GP4dcsM.jpg" alt="Book cover for Competitive Programmer's Handbook"> </book-cover>

This book is a modern introduction to competitive programming and discusses programming tricks and algorithm design techniques relevant in competitive programming.

[Author : Antti Laaksonen - GitHub](https://github.com/pllk)

<cta-button text="Complete Book" link="https://cses.fi/book/book.pdf"></cta-button>


### Principles of Algorithmic Problem Solving

<book-cover link="https://www.csc.kth.se/~jsannemo/slask/main.pdf" img-src="https://i.imgur.com/D0Bplv4.png" alt="Book cover for Principles of Algorithmic Problem Solving"> </book-cover>

This book is oriented towards preparing for competitive programming competitions. It covers many fundamental algorithm types and commonly studied algorithms. The exercises are links to Kattis free online judging system. The book has examples in C++ as well as a short introduction

[Author : Johan Sannemo - GitHub](https://github.com/jsannemo)

<cta-button text="Complete Book" link="https://www.csc.kth.se/~jsannemo/slask/main.pdf"></cta-button>

</br>

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#competitive-programming"></cta-button>


## Graphics Programming

### 3D Game Shaders For Beginners 

<book-cover link="https://github.com/lettier/3d-game-shaders-for-beginners" img-src="https://i.imgur.com/KQS4TS6.jpg" alt="Book cover for 3D Game Shaders For Beginners "> </book-cover>

A step-by-step guide to implementing SSAO, depth of field, lighting, normal mapping, and more for your 3D game.

[Author : David Lettier - GitHub](https://github.com/lettier)

<cta-button text="Complete Book" link="https://github.com/lettier/3d-game-shaders-for-beginners"></cta-button>


### Blender 3D: Noob to Pro 

<book-cover link="https://en.wikibooks.org/wiki/Blender_3D%3A_Noob_to_Pro" img-src="https://i.imgur.com/z9hZmnA.png" alt="Book cover for Blender 3D: Noob to Pro "> </book-cover>

This guide is a product of shared effort by numerous team members and anonymous editors. Its purpose is to teach people how to create three-dimensional computer graphics using Blender, a free software application.

[Source : Wikibooks.org](https://en.wikibooks.org/wiki/Main_Page)

<cta-button text="Complete Book" link="https://en.wikibooks.org/wiki/Blender_3D%3A_Noob_to_Pro"></cta-button>


### Notes for a Computer Graphics Programming Course 

<book-cover link="https://www.cs.csustan.edu/~rsc/CS3600F00/Notes.pdf" img-src="https://i.imgur.com/7gV7gCV.png" alt="Book cover for Notes for a Computer Graphics Programming Course"> </book-cover>

These notes cover topics in an introductory computer graphics course that emphasizes graphics programming.

Its goal is to introduce fundamental concepts and processes for computer graphics, as well as giving students experience in computer graphics programming using the OpenGL application programming interface (API).

[Author : Dr. Steve Cunningham ](https://www.cs.csustan.edu/~rsc/)

<cta-button text="Complete Book" link="https://www.cs.csustan.edu/~rsc/CS3600F00/Notes.pdf"></cta-button>

</br>

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#graphics-programming"></cta-button>


## Regular Expressions

### JavaScript RegExp

<book-cover link="https://learnbyexample.github.io/learn_js_regexp/" img-src="https://i.imgur.com/9p6laqA.png" alt="Book cover for JavaScript RegExp"> </book-cover>

This book will help you learn Regular Expressions as implemented in JavaScript. Regular expressions can be considered as a mini-programming language in itself and is well suited for a variety of text processing needs.

[Author : Sundeep Agarwal - GitHub](https://github.com/learnbyexample)

<cta-button text="Complete Book" link="https://learnbyexample.github.io/learn_js_regexp/"></cta-button>


### Python re(gex)? 

<book-cover link="https://learnbyexample.github.io/py_regular_expressions/" img-src="https://i.imgur.com/f71HmXh.png" alt="Book cover for Python re(gex)?"> </book-cover>

This book will help you learn Regular Expressions, a mini-programming language for all sorts of text processing needs.

[Author : Sundeep Agarwal - GitHub](https://github.com/learnbyexample)

<cta-button text="Complete Book" link="https://learnbyexample.github.io/py_regular_expressions/"></cta-button>


</br>

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#regular-expressions"></cta-button>


---
title: Cheat Sheats
description:  We're compiling a library of useful programming languages' cheat sheets from around the Internet to help you on your software journey!
position: 12506
category: Programming
---


## APL

### A Reference Card For GNU APL

[![A reference card for GNU APL Preview](https://i.imgur.com/89LhvgQ.png)](https://github.com/jpellegrini/gnu-apl-refcard/blob/master/aplcard.pdf)

[Source : jpellegrini - Github](https://github.com/jpellegrini)

<cta-button text="View" link="https://github.com/jpellegrini/gnu-apl-refcard/blob/master/aplcard.pdf"></cta-button>

### Dyalog APL - Vocabulary

[![Dyalog APL - Vocabulary Preview](https://i.imgur.com/jVUBwn4.png)](https://awagga.github.io/dyalog/voc/)

[Source : awagga - Github](https://github.com/awagga/awagga.github.io)

<cta-button text="View" link="https://awagga.github.io/dyalog/voc/"></cta-button>

</br>

<cta-button text="More Cheatsheets On APL" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#apl"></cta-button>


## Bash

### Bash Cheatsheet

[![Bash Cheatsheet Preview](https://i.imgur.com/XdmNn1y.png)](https://www.cheatsheet.wtf/bash/)

[Source : cheatsheet.wtf](https://www.cheatsheet.wtf/)

<cta-button text="View" link="https://www.cheatsheet.wtf/bash/"></cta-button>


### Bash Scripting cheatsheet

[![Bash Scripting cheatsheet Preview](https://i.imgur.com/5R2GuOM.png)](https://devhints.io/bash)

[Source : devhints.io](https://devhints.io/)

<cta-button text="View" link="https://devhints.io/bash"></cta-button>

</br>

<cta-button text="More Cheatsheets On Bash" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#bash"></cta-button>


## C#

### C# Cheat Sheet

[![C# Cheat Sheet Preview](https://i.imgur.com/BRcCD54.png)](https://simplecheatsheet.com/tag/c-cheat-sheet-1/)

[Source : simplecheatsheet.com](https://simplecheatsheet.com/)

<cta-button text="View" link="https://simplecheatsheet.com/tag/c-cheat-sheet-1/"></cta-button>

</br>

<cta-button text="More Cheatsheets On C#" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#csharp"></cta-button>


## C++

### C++ Cheatsheet

[![C++ Cheatsheet Preview](https://i.imgur.com/aTejOjI.png)](https://www.codewithharry.com/blogpost/cpp-cheatsheet)

[Source : codewithharry.com](https://www.codewithharry.com/)

<cta-button text="View" link="https://www.codewithharry.com/blogpost/cpp-cheatsheet"></cta-button>


### C++ Quick Reference

[![C++ Quick Reference Preview](https://i.imgur.com/6CETuOd.png)](http://www.hoomanb.com/cs/quickref/CppQuickRef.pdf)

[Source : hoomanb.com](http://www.hoomanb.com/cs/)

<cta-button text="View" link="http://www.hoomanb.com/cs/quickref/CppQuickRef.pdf"></cta-button>

</br>

<cta-button text="More Cheatsheets On C++" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#cpp"></cta-button>

## C

### C Reference Card (ANSI)

[![C Reference Card (ANSI) Preview](https://i.imgur.com/y4CDVSD.png)](https://users.ece.utexas.edu/~adnan/c-refcard.pdf)

[Author : Joseph H. Silverman](http://www.math.brown.edu/johsilve/)

<cta-button text="View" link="https://users.ece.utexas.edu/~adnan/c-refcard.pdf"></cta-button>


### Systems Programming Cheat Sheet

[![Systems Programming Cheat Sheet Preview](https://i.imgur.com/G8aA8NX.png)](https://github.com/jstrieb/systems-programming-cheat-sheet)

[Author : Jacob Strieb - Github ](https://github.com/jstrieb)

<cta-button text="View" link="https://github.com/jstrieb/systems-programming-cheat-sheet"></cta-button>


### The C Cheat Sheet: An Introduction to Programming in C 

[![The C Cheat Sheet: An Introduction to Programming in C Preview ](https://i.imgur.com/rJIKdik.png)](https://sites.ualberta.ca/~ygu/courses/geoph624/codes/C.CheatSheet.pdf)

[Author : Andrew Sterian](https://www.linkedin.com/in/steriana/)

<cta-button text="View" link="https://sites.ualberta.ca/~ygu/courses/geoph624/codes/C.CheatSheet.pdf"></cta-button>

</br>

<cta-button text="More Cheatsheets On C " link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#c"></cta-button>


## Clojure

### Clojure Cheatsheet

[![Clojure Cheatsheet Preview ](https://i.imgur.com/sfZGssV.png)](https://clojure.org/api/cheatsheet)

[Source : clojure.org](https://clojure.org/index)

<cta-button text="View" link="https://clojure.org/api/cheatsheet"></cta-button>

</br>

<cta-button text="More Cheatsheets On Clojure" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#clojure"></cta-button>


## Data Science

### Cheatsheets for Data Scientists

[![Cheatsheets for Data Scientists - Datacamp (PDF)](https://i.imgur.com/1iZncpj.png)](https://www.datacamp.com/community/data-science-cheatsheets)

[Source : datacamp.com](https://www.datacamp.com/)

<cta-button text="View" link="https://www.datacamp.com/community/data-science-cheatsheets"></cta-button>

</br>

<cta-button text="More Cheatsheets On Data Science" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#data-science"></cta-button>


## Docker

### Docker Cheat Sheet

[![Docker Cheat Sheet Preview ](https://i.imgur.com/lfWiVBS.png)](https://low-orbit.net/docker-cheat-sheet)

[Source : Low Orbit Flux](https://low-orbit.net/)

<cta-button text="View" link="https://low-orbit.net/docker-cheat-sheet"></cta-button>


### Docker Security Cheat Sheet

[![Docker Security Cheat Sheet Preview](https://i.imgur.com/Oz8S9np.png)](https://cheatsheetseries.owasp.org/cheatsheets/Docker_Security_Cheat_Sheet.html)

[Source : cheatsheetseries.owasp.org](https://cheatsheetseries.owasp.org/index.html)

<cta-button text="View" link="https://cheatsheetseries.owasp.org/cheatsheets/Docker_Security_Cheat_Sheet.html"></cta-button>


### Docker Cheatsheet: Docker commands that developers should know

[![Docker Cheatsheet: Docker commands that developers should know Preview](https://i.imgur.com/CUp4cgj.png)](https://vishnuch.tech/docker-cheatsheet)

[Author: Vishnu Chilamakuru](https://vishnuch.tech/)

<cta-button text="View" link="https://vishnuch.tech/docker-cheatsheet"></cta-button>

</br>

<cta-button text="More Cheatsheets On Docker" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#docker"></cta-button>


## Git

### Git Cheat Sheet - GitHub

[![Git Cheat Sheet - GitHub Preview](https://i.imgur.com/zsDNuTc.png)](https://education.github.com/git-cheat-sheet-education.pdf)

[Source : education.github.com](https://education.github.com/)

<cta-button text="View" link="https://education.github.com/git-cheat-sheet-education.pdf"></cta-button>


### Git Cheat Sheet - GitLab 

[![Git Cheat Sheet - GitLab Preview](https://i.imgur.com/ALrKLSM.png)](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)

[Author: about.gitlab.com](https://about.gitlab.com/)

<cta-button text="View" link="https://about.gitlab.com/images/press/git-cheat-sheet.pdf"></cta-button>


### GitHub Cheat Sheet

[![GitHub Cheat Sheet Preview](https://i.imgur.com/SvbJnct.png)](https://github.com/tiimgreen/github-cheat-sheet)

[Author: Tim Green - GitHub](https://github.com/tiimgreen)

<cta-button text="View" link="https://github.com/tiimgreen/github-cheat-sheet"></cta-button>

</br>

<cta-button text="More Cheatsheets On Git" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#git"></cta-button>


## Go

### Go Cheatsheet

[![Go Cheatsheet - preview ](https://i.imgur.com/NlKXeDS.png)](https://cht.sh/go/:learn)

[Author: Igor Chubin - GitHub](https://github.com/chubin)

<cta-button text="View" link="https://cht.sh/go/:learn"></cta-button>

</br>

<cta-button text="More Cheatsheets On Go" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#go"></cta-button>


## HTML and CSS

### HTML & CSS Emmet Cheat Sheet

[![HTML & CSS Cheat Sheet Preview](https://i.imgur.com/CoSSmyn.png)](https://docs.emmet.io/cheat-sheet/)

[Author: Emmet](https://docs.emmet.io/)

<cta-button text="View" link="https://docs.emmet.io/cheat-sheet/"></cta-button>


### CSS Flexbox Cheatsheet 

[![CSS Flexbox Cheatsheet Preview ](https://i.imgur.com/OjikWda.png)](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)

[Author: Chris Coyier](https://css-tricks.com/author/chriscoyier/)

<cta-button text="View" link="https://css-tricks.com/snippets/css/a-guide-to-flexbox/"></cta-button>


### CSS Grid Cheatsheet 

[![CSS Grid Cheatsheet Preview](https://i.imgur.com/Swjmu0r.png)](https://css-tricks.com/snippets/css/complete-guide-grid/)

[Author: Chris House](https://css-tricks.com/author/caiman/)

<cta-button text="View" link="https://css-tricks.com/snippets/css/complete-guide-grid/"></cta-button>

</br>

<cta-button text="More Cheatsheets On HTML and CSS" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#html--css"></cta-button>


## Java

### Java Complete Cheat Sheet

[![Java Complete Cheat Sheet Preview ](https://i.imgur.com/TzrtWuD.png)](https://programmingwithmosh.com/wp-content/uploads/2019/07/Java-Cheat-Sheet.pdf)

[Author: Mosh Hamedani](https://codewithmosh.com/)

<cta-button text="View" link="https://programmingwithmosh.com/wp-content/uploads/2019/07/Java-Cheat-Sheet.pdf"></cta-button>


### Java Cheatsheet 

[![Java Cheatsheet Preview](https://i.imgur.com/TNT571t.png)](https://www.codewithharry.com/blogpost/java-cheatsheet)

[Author: Code With Harry](https://www.codewithharry.com/)

<cta-button text="View" link="https://www.codewithharry.com/blogpost/java-cheatsheet"></cta-button>

</br>

<cta-button text="More Cheatsheets On Java" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#java"></cta-button>


## Javascript 

### JavaScript Complete Cheatsheet 

[![JavaScript Cheatsheet Preview](https://i.imgur.com/lGLytvN.png)](https://www.codecademy.com/learn/introduction-to-javascript/modules/learn-javascript-introduction/cheatsheet)

[Source : codecademy.com](https://www.codecademy.com/)

<cta-button text="View" link="https://www.codecademy.com/learn/introduction-to-javascript/modules/learn-javascript-introduction/cheatsheet"></cta-button>


### JavaScript CheatSheet 

[![JavaScript CheatSheet Preview](https://i.imgur.com/zRMvBux.png)](https://htmlcheatsheet.com/js/)

[Source: htmlcheatsheet.com](https://htmlcheatsheet.com/)

<cta-button text="View" link="https://htmlcheatsheet.com/js/"></cta-button>

</br>

<cta-button text="More Cheatsheets On JavaScript" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#javascript"></cta-button>


## jQuery 

### jQuery CheatSheet 

[![jQuery CheatSheet Preview](https://i.imgur.com/8kMvfUW.png)](https://htmlcheatsheet.com/jquery/)

[Source: htmlcheatsheet.com](https://htmlcheatsheet.com/)

<cta-button text="View" link="https://htmlcheatsheet.com/jquery/"></cta-button>

</br>

<cta-button text="More Cheatsheets On jQuery" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#jquery"></cta-button>


## Nest.js

### Nest.js CheatSheet 

[![Nest.js CheatSheet Preview](https://i.imgur.com/KjNyuCM.png)](https://gist.github.com/guiliredu/0aa9e4d338bbeeac369a597e87c9ba46)

[Author : Guilherme Dias Redü - GitHub Gist](https://gist.github.com/guiliredu)

<cta-button text="View" link="https://gist.github.com/guiliredu/0aa9e4d338bbeeac369a597e87c9ba46"></cta-button>

</br>

<cta-button text="More cheatsheets On Nest.js" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#nestjs"></cta-button>


## Nuxt.js

### Nuxt.js Essentials Cheatsheet

[![Nuxt.js Essentials Cheatsheet Preview](https://i.imgur.com/lkXcqgF.png)](https://www.vuemastery.com/pdf/Nuxtjs-Cheat-Sheet.pdf)

[Source : vuemastery.com](https://www.vuemastery.com/)

<cta-button text="View" link="https://www.vuemastery.com/pdf/Nuxtjs-Cheat-Sheet.pdf"></cta-button>

</br>

<cta-button text="More Cheatsheets On Nuxt.js" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#nuxtjs"></cta-button>


## React.js

### React Cheatsheet 

[![React Cheatsheet Preview](https://i.imgur.com/Tf3MAVU.png)](https://www.codecademy.com/learn/react-101/modules/react-101-jsx-u/cheatsheet)

[Source : codecademy.com](https://www.codecademy.com/)

<cta-button text="View" link="https://www.codecademy.com/learn/react-101/modules/react-101-jsx-u/cheatsheet"></cta-button>

</br>

<cta-button text="More Cheatsheets On React.js" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#reactjs"></cta-button>


## Vue.js

### Vue Essential Cheatsheet 

[![Vue Essential Cheatsheet Preview](https://i.imgur.com/c1G4rLb.png)](https://www.vuemastery.com/pdf/Vue-Essentials-Cheat-Sheet.pdf)

[Source : vuemastery.com](https://www.vuemastery.com/)

<cta-button text="View" link="https://www.vuemastery.com/pdf/Vue-Essentials-Cheat-Sheet.pdf"></cta-button>


### Vue.js 2.3 Complete API Cheat Sheet

[![Vue.js 2.3 Complete API Cheat Sheet Preview](https://i.imgur.com/4iQND0z.png)](https://marozed.com/vue-cheatsheet)

[Author : Marcos Neves - GitHub](https://github.com/neves)

<cta-button text="View" link="https://marozed.com/vue-cheatsheet"></cta-button>

</br>

<cta-button text="More Cheatsheets On Vue.js" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#vuejs"></cta-button>


## Kotlin

### Kotlin Cheatsheet and Quick Reference

[![Kotlin Cheatsheet and Quick Reference Preview](https://i.imgur.com/JAyqVO6.png)](https://koenig-media.raywenderlich.com/uploads/2019/11/RW-Kotlin-Cheatsheet-1.1.pdf)

[Source : raywenderlich.com](https://www.raywenderlich.com/)

<cta-button text="View" link="https://koenig-media.raywenderlich.com/uploads/2019/11/RW-Kotlin-Cheatsheet-1.1.pdf"></cta-button>

</br>

<cta-button text="More Cheatsheets On Kotlin" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#kotlin"></cta-button>


## Kubernetes

### Handy Cheat Sheet for Kubernetes Beginners 

[![Handy Cheat Sheet for Kubernetes Beginners Preview](https://i.imgur.com/4BbpKtt.png)](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)

[Source : kubernetes.io](https://kubernetes.io/)

<cta-button text="View" link="https://kubernetes.io/docs/reference/kubectl/cheatsheet/"></cta-button>

</br>

<cta-button text="More Cheatsheets On Kubernetes" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#kubernetes"></cta-button>


## Language Translations

### Swift and C# Quick Reference - Language Equivalents and Code Examples

[![Swift and C# Quick Reference - Language Equivalents and Code Examples Preview](https://i.imgur.com/casm9GG.png)](https://www.globalnerdy.com/wordpress/wp-content/uploads/2015/03/SwiftCSharpPoster.pdf)

[Author : Joey deVilla](https://www.globalnerdy.com/about/)

<cta-button text="View" link="https://www.globalnerdy.com/wordpress/wp-content/uploads/2015/03/SwiftCSharpPoster.pdf"></cta-button>

</br>

<cta-button text="More Cheatsheets On Language Translations" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#language-translations"></cta-button>


## Markdown

### Markdown Cheat Sheet - Markdown Guide

[![Markdown Cheat Sheet - Markdown Guide Preview](https://i.imgur.com/Ih3uFtz.png)](https://www.markdownguide.org/cheat-sheet/)

[Source : markdownguide.org](https://www.markdownguide.org/)

<cta-button text="View" link="https://www.markdownguide.org/cheat-sheet/"></cta-button>


### Markdown Here 

[![Markdown Here Preview](https://i.imgur.com/kcB3rul.png)](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

[Author : Adam Pritchard - GitHub](https://github.com/adam-p)

<cta-button text="View" link="https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet"></cta-button>

</br>

<cta-button text="More Cheatsheets On Markdown" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#markdown"></cta-button>


## MATLAB

### MATLAB Basic Functions Reference Sheet 

[![MATLAB Basic Functions Reference Sheet Preview](https://i.imgur.com/JfV5rc4.png)](https://www.mathworks.com/content/dam/mathworks/fact-sheet/matlab-basic-functions-reference.pdf)

[Source : mathworks.com](https://www.mathworks.com/)

<cta-button text="View" link="https://www.mathworks.com/content/dam/mathworks/fact-sheet/matlab-basic-functions-reference.pdf"></cta-button>

</br>

<cta-button text="More Cheatsheets On Matlab" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#matlab"></cta-button>


## MongoDB

### MongoDB Cheat Sheet 

[![MongoDB Cheat Sheet Preview](https://i.imgur.com/CgmD0dR.png)](https://www.mongodb.com/developer/quickstart/cheat-sheet/)

[Source : mongodb.com](https://www.mongodb.com/)

<cta-button text="View" link="https://www.mongodb.com/developer/quickstart/cheat-sheet/"></cta-button>


### MongoDB Cheat Sheet 

[![MongoDB Cheat Sheet  Preview](https://i.imgur.com/ZbPqJs3.png)](https://blog.codecentric.de/files/2012/12/MongoDB-CheatSheet-v1_0.pdf)

[Source : codecentric.de](https://www.codecentric.de/)

<cta-button text="View" link="https://blog.codecentric.de/files/2012/12/MongoDB-CheatSheet-v1_0.pdf"></cta-button>


### Quick Cheat Sheet For Mongo DB Shell Commands

[![Quick Cheat Sheet for Mongo DB Shell commands Preview](https://i.imgur.com/jEFaKMS.png)](https://gist.github.com/michaeltreat/d3bdc989b54cff969df86484e091fd0c)

[Author : MongoDB - GitHub](https://gist.github.com/michaeltreat)

<cta-button text="View" link="https://gist.github.com/michaeltreat/d3bdc989b54cff969df86484e091fd0c"></cta-button>

</br>

<cta-button text="More Cheatsheets On MongoDB" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#mongodb"></cta-button>


## Perl

### Perl Reference Card

[![Perl Reference card Preview](https://i.imgur.com/oQb38Hv.png)](https://michaelgoerz.net/refcards/perl_refcard.pdf)

[Author : Michael Goerz - GitHub](https://github.com/goerz)

<cta-button text="View" link="https://michaelgoerz.net/refcards/perl_refcard.pdf"></cta-button>

<cta-button text="More Cheatsheets On Perl" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#perl"></cta-button>


## PyTorch

### PyTorch Framework Cheat Sheet

[![PyTorch Framework Cheat Sheet Preview](https://i.imgur.com/FlfCC31.png)](https://www.simonwenkel.com/publications/cheatsheets/pdf/cheatsheet_pytorch.pdf)

[Author : Simon Wenkel](https://www.simonwenkel.com/)

<cta-button text="View" link="https://www.simonwenkel.com/publications/cheatsheets/pdf/cheatsheet_pytorch.pdf"></cta-button>


### PyTorch Official Cheat Sheet

[![PyTorch Official Cheat Sheet Preview](https://i.imgur.com/xZkTAKY.png)](https://pytorch.org/tutorials/beginner/ptcheat.html)

[Source : pytorch.org](https://pytorch.org/)

<cta-button text="View" link="https://pytorch.org/tutorials/beginner/ptcheat.html"></cta-button>

</br>

<cta-button text="More Cheatsheets On PyTorch" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#pytorch"></cta-button>


## PHP

### PHP Cheat Sheet 

[![PHP Cheat Sheet Preview](https://i.imgur.com/7w7BeSu.png)](https://websitesetup.org/php-cheat-sheet/)

[Author : Nick Schaferhoff ](https://nickschaeferhoff.com/)

<cta-button text="View" link="https://websitesetup.org/php-cheat-sheet/"></cta-button>

</br>

<cta-button text="More Cheatsheets On PHP" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#php"></cta-button>



## Python

### Comprehensive Python Cheatsheet 

[![Comprehensive Python Cheatsheet Preview](https://i.imgur.com/8UJGlg6.png)](https://gto76.github.io/python-cheatsheet/)

[Author: Jure Šorn](https://gto76.github.io/)

<cta-button text="View" link="https://gto76.github.io/python-cheatsheet/"></cta-button>


### Python Crash Course - Cheat Sheets

[![Python Crash Course - Cheat Sheets](https://i.imgur.com/wDqjHLl.png)](https://ehmatthes.github.io/pcc/cheatsheets/README.html)

[Author: Eric Matthes](https://ehmatthes.github.io/)

<cta-button text="View" link="https://ehmatthes.github.io/pcc/cheatsheets/README.html"></cta-button>


### Python Cheat Sheet

[![Python Cheat Sheet](https://i.imgur.com/zBBo00o.png)](https://cheatography.com/davechild/cheat-sheets/python/)

[Source: Cheatography.com](https://cheatography.com/)

<cta-button text="View" link="https://cheatography.com/davechild/cheat-sheets/python/"></cta-button>


### Python for Data Science Cheatsheets

[![Python Cheat Sheet](https://i.imgur.com/hEUK4cm.jpg)](https://pydatascience.org/data-science-cheatsheets/)

[Source: Pydatascience.org](https://pydatascience.org/)

<cta-button text="View" link="https://pydatascience.org/data-science-cheatsheets/"></cta-button>


</br>

<cta-button text="More Cheatsheets On Python" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#python"></cta-button>


## R

### All RStudio cheatsheets resources 

[![All RStudio cheatsheets resources Preview](https://i.imgur.com/D97iUyJ.png)](https://www.rstudio.com/resources/cheatsheets/)

[Source : Rstudio.com](https://www.rstudio.com/)

<cta-button text="View" link="https://www.rstudio.com/resources/cheatsheets/"></cta-button>

</br>

<cta-button text="More Cheatsheets On R" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#r"></cta-button>


## Raspberry Pi

### Basic GPIO layout configuration cheatsheet

[![Basic GPIO layout configuration cheatsheet Preview ](https://i.imgur.com/gB97Jep.png)](https://www.cl.cam.ac.uk/projects/raspberrypi/tutorials/robot/cheat_sheet/)

[Source : University of Cambridge - Department of Computer Science and Technology](https://www.cl.cam.ac.uk/projects/raspberrypi/tutorials/)

<cta-button text="View" link="https://www.cl.cam.ac.uk/projects/raspberrypi/tutorials/robot/cheat_sheet/"></cta-button>


### Raspberry Pi Basics cheatsheet 

[![Raspberry Pi Basics cheatsheet  Preview](https://i.imgur.com/8rRHEM5.png)](https://www.woolseyworkshop.com/wp-content/uploads/WoolseyWorkshop_Cheatsheet_RaspberryPiBasics_v1.4.pdf)

[Source: woolseyworkshop.com](https://www.woolseyworkshop.com/)

<cta-button text="View" link="https://www.woolseyworkshop.com/wp-content/uploads/WoolseyWorkshop_Cheatsheet_RaspberryPiBasics_v1.4.pdf"></cta-button>

</br>

<cta-button text="More Cheatsheets On Raspberry Pi" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#raspberry-pi"></cta-button>


## Ruby

### Ruby Cheat Sheet 

[![Ruby Cheat Sheet Preview](https://i.imgur.com/wIbmUiG.png)](https://www.codeconquest.com/wp-content/uploads/Ruby-Cheat-Sheet-by-CodeConquestDOTcom.pdf)

[Source : CodeConquest.com](https://www.codeconquest.com/)

<cta-button text="View" link="https://www.codeconquest.com/wp-content/uploads/Ruby-Cheat-Sheet-by-CodeConquestDOTcom.pdf"></cta-button>

</br>

<cta-button text="More Cheatsheets On Ruby" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#ruby"></cta-button>


## Rust

### Rust Language Cheat Sheet 

[![Rust Language Cheat Sheet Preview](https://i.imgur.com/gBVUd6M.png)](https://cheats.rs/)

[Source : cheats.rs](https://cheats.rs/)

<cta-button text="View" link="https://cheats.rs/"></cta-button>

</br>

<cta-button text="More Cheatsheets On Rust" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#rust"></cta-button>


## Solidity

### Solidity Cheat Sheet 

[![Solidity Cheat Sheet Preview ](https://i.imgur.com/1vqrchn.png)](https://intellipaat.com/mediaFiles/2019/03/Solidity-Cheat-Sheet.pdf)

[Source : intellipaat.com](https://intellipaat.com/)

<cta-button text="View" link="https://intellipaat.com/mediaFiles/2019/03/Solidity-Cheat-Sheet.pdf"></cta-button>


### Solidity Cheatsheet and Best practices 

[![Solidity Cheatsheet and Best practices Preview ](https://i.imgur.com/M8VXV56.png)](https://manojpramesh.github.io/solidity-cheatsheet/)

[Author : Manoj Ramesh - GitHub](https://github.com/manojpramesh)

<cta-button text="View" link="https://manojpramesh.github.io/solidity-cheatsheet/"></cta-button>

</br>

<cta-button text="More Cheatsheets On Solidity" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#solidity"></cta-button>


## SQL

### MySQL Cheatsheet

[![MySQL Cheatsheet Preview](https://i.imgur.com/ze2ICnl.png)](https://s3-us-west-2.amazonaws.com/dbshostedfiles/dbs/sql_cheat_sheet_mysql.pdf)

[Source: databasestar.com ](https://www.databasestar.com/)

<cta-button text="View" link="https://s3-us-west-2.amazonaws.com/dbshostedfiles/dbs/sql_cheat_sheet_mysql.pdf"></cta-button>


### PostgreSQL Cheatsheet 

[![PostgreSQL Cheatsheet Preview](https://i.imgur.com/GOJo3PT.png)](https://s3-us-west-2.amazonaws.com/dbshostedfiles/dbs/sql_cheat_sheet_pgsql.pdf)

[Source: databasestar.com ](https://www.databasestar.com/)

<cta-button text="View" link="https://s3-us-west-2.amazonaws.com/dbshostedfiles/dbs/sql_cheat_sheet_pgsql.pdf"></cta-button>


### SQL Basics Cheat Sheet

[![SQL Cheat Sheet Preview](https://i.imgur.com/kN3t2U1.png)](https://learnsql.com/blog/sql-basics-cheat-sheet/sql-basics-cheat-sheet-a4.pdf)

[Source: learnsql.com](https://learnsql.com/)

<cta-button text="View" link="https://learnsql.com/blog/sql-basics-cheat-sheet/sql-basics-cheat-sheet-a4.pdf"></cta-button>


</br>

<cta-button text="More Cheatsheets On SQL" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#sql"></cta-button>


## Tensorflow 

### TensorFlow Quick Reference Table

[![TensorFlow Quick Reference Table Preview](https://i.imgur.com/ZLW4Tzm.png)](https://secretdatascientist.com/tensor-flow-cheat-sheet/)

[Source : Secretdatascientist.com](https://secretdatascientist.com/)

<cta-button text="View" link="https://secretdatascientist.com/tensor-flow-cheat-sheet/"></cta-button>


### TensorFlow v2.0 Cheat Sheet

[![TensorFlow v2.0 Cheat Sheet Preview](https://i.imgur.com/PueJwJj.png)](https://web.archive.org/web/20200922212358/https://www.aicheatsheets.com/static/pdfs/tensorflow_v_2.0.pdf)

[Source : Altoros.com/visuals](https://www.altoros.com/visuals)

<cta-button text="View" link="https://web.archive.org/web/20200922212358/https://www.aicheatsheets.com/static/pdfs/tensorflow_v_2.0.pdf"></cta-button>

</br>

<cta-button text="More Cheatsheets On Tensorflow" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#tensorflow"></cta-button>
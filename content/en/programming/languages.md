---
title: Programming Languages
description:  We're compiling a library of useful resources for numerous programming languages to help you on your software journey!
position: 12504
category: Programming
---


## ABAP

- [SAP Code Style Guides - Clean ABAP](https://github.com/SAP/styleguides/blob/main/clean-abap/CleanABAP.md)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#abap"></cta-button>


## ADA

- [A Guide to Ada for C and C++ Programmers (PDF)](http://www.cs.uni.edu/~mccormic/4740/guide-c2ada.pdf)

- [Ada Distilled (PDF)](http://www.adapower.com/pdfs/AdaDistilled07-27-2003.pdf)

- [Ada for the C++ or Java Developer - Quentin Ochem (PDF)](https://www.adacore.com/uploads/books/pdf/Ada_for_the_C_or_Java_Developer-cc.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#ada"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#ada"></cta-button>


## Agda

- [Agda Tutorial](https://people.inf.elte.hu/divip/AgdaTutorial/Index.html)

- [Programming Language Foundations in Agda - Philip Wadler and Wen Kokke](https://plfa.github.io/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#agda"></cta-button>


## Alef

- [Alef Language Reference Manual](http://doc.cat-v.org/plan_9/2nd_edition/papers/alef/ref)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#alef"></cta-button>


## Android

- [Android Notes for Professionals - Compiled from StackOverflow Documentation (PDF)](https://goalkicker.com/AndroidBook/)

- [Android Tutorial - Tutorials Point (HTML, PDF)](https://www.tutorialspoint.com/android/)

- [Codelabs for Advanced Android Development](https://developer.android.com/courses/advanced-training/toc)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#android"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#android"></cta-button>


## APL

- [Learning APL - Stefan Kruger (HTML,PDF,IPYNB)](https://xpqz.github.io/learnapl/intro.html)

- [A Practical Introduction to APL1 & APL2 - Graeme Donald Robertson (PDF)](http://robertson.uk.net/Files/APL1&2.pdf)

- [A Practical Introduction to APL3 & APL4 - Graeme Donald Robertson (PDF)](http://robertson.uk.net/Files/APL3&4.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#apl"></cta-button>


## App Inventor

- [Absolute App Inventor 2 - Hossein Amerkashi](https://amerkashi.wordpress.com/2015/02/16/absolute-app-inventor-2-book/)

- [App Inventor 2 - David Wolber, Hal Abelson, Ellen Spertus, Liz Looney](http://www.appinventor.org/book2)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#app-inventor"></cta-button>


## Arduino

- [Introduction to Arduino](https://playground.arduino.cc/Main/ManualsAndCurriculum/)

- [Introduction to Arduino : A piece of cake! - Alan G. Smith](http://www.introtoarduino.com/)

- [Arduino Tips, Tricks, and Techniques - lady ada (PDF)](https://cdn-learn.adafruit.com/downloads/pdf/arduino-tips-tricks-and-techniques.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#arduino"></cta-button>


## ASP.NET

- [Intro to ASPNET MVC 4 with Visual Studio 2011 Beta (2012) - Rick Anderson and Scott Hanselman (PDF)](http://www.vijaymukhi.com/documents/books/vsnet/content.htm)

- [Diving into ASP.NET WebAPI (2016) - Akhil Mittal (PDF)](https://github.com/akhilmittal/FreeBooks/)

- [ASP.NET WebHooks Succinctly - Gaurav Arora](https://www.syncfusion.com/succinctly-free-ebooks/aspnet-webhooks-succinctly)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#aspnet"></cta-button>


## ASP.NET Core

- [ASP.NET Core Documentation - Microsoft Docs](https://docs.microsoft.com/en-us/aspnet/core/?view=aspnetcore-5.0)

- [The Little ASP.NET Core Book (2018) - Nate Barbettini (PDF)](https://s3.amazonaws.com/recaffeinate-files/LittleAspNetCoreBook.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#aspnet-core"></cta-button>


### Blazor

- [Blazor: A Beginner's Guide - Ed Charbeneau (PDF) (email address requested, not required)](https://www.telerik.com/campaigns/blazor/wp-beginners-guide-ebook)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#blazor"></cta-button>


## Assembly Language

- [A fundamental introduction to x86 assembly prorgamming - Project Nayuki (HTML)](https://www.nayuki.io/page/a-fundamental-introduction-to-x86-assembly-programming)

- [Programming from the Ground Up - Jonathan Bartlett (PDF)](https://download-mirror.savannah.gnu.org/releases/pgubook/ProgrammingGroundUp-1-0-booksize.pdf)

- [WebAssembly friendly programming with C/C++ - Ending, Chai Shushan, Yushih](https://github.com/3dgen/cppwasm-book/tree/master/en) with [examples](https://github.com/3dgen/cppwasm-book/tree/master/examples)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#assembly-language"></cta-button>


### Non-X86

- [Easy 6502 - Nick Morgan](http://skilldrick.github.io/easy6502/)
  
- [Programmed Introduction to MIPS Assembly Language](https://chortle.ccsu.edu/AssemblyTutorial/index.html)

- [Machine Language for Beginners - Richard Mansfield [6502 CPU]](https://archive.org/details/ataribooks-machine-language-for-beginners)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#non-x86"></cta-button>


## AutoHotkey

- [AutoHotkey Official Documentation (CHM)](https://www.autohotkey.com/docs/AutoHotkey.htm)

- [AHKbook - the book for AutoHotkey](http://ahkscript.github.io/ahkbook/en/Introduction.html)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#autohotkey"></cta-button>


## AutoIt

- [AutoIt Docs - Jonathan Bennett (HTML)](https://www.autoitscript.com/autoit3/docs/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#autoit"></cta-button>


## Autotools

- [Autotools Mythbuster](https://autotools.info/index.html)

- [GNU Autoconf, Automake and Libtool](http://sourceware.org/autobook/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#autotools"></cta-button>


## Awk

- [Awk - Bruce Barnett](https://www.grymoire.com/Unix/Awk.html)

- [Gawk: Effective AWK Programming - Arnold D. Robbins (HTML, PDF)](https://www.gnu.org/software/gawk/manual/)

- [GNU awk - Sundeep Agarwal](https://learnbyexample.github.io/learn_gnuawk/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#awk"></cta-button>


## Bash

- [Bash Guide for Beginners (2008) - M. Garrels (HTML)](https://tldp.org/LDP/Bash-Beginners-Guide/html/)

- [Advanced Bash-Scripting Guide - M. Cooper (HTML)](https://tldp.org/LDP/abs/html/)

- [Bash Notes for Professionals - Compiled from StackOverflow documentation (PDF)](https://goalkicker.com/BashBook/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#bash"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#bash"></cta-button>


## Basic

- [A beginner's guide to Gambas - John W. Rittinghouse (PDF)](http://distro.ibiblio.org/vectorlinux/Uelsk8s/GAMBAS/gambas-beginner-guide.pdf)

- [Pick/Basic: A Programmer's Guide - Jonathan E. Sisk](https://secure28.securewebsession.com/jes.com/pb/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#basic"></cta-button>


## BeanShell

- [Beanshell Simple Java Scripting Manual (PDF)](http://www.beanshell.org/manual/bshmanual.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#beanshell"></cta-button>


## BETA

- [MIA 94-26: BETA Language Introduction](https://beta.cs.au.dk/Manuals/latest/beta-intro/index.html)

- [Object-Oriented Programming in the BETA Programming Language ](https://beta.cs.au.dk/Books/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#beta"></cta-button>


## C++

- [C++ Core Guidelines - Editors: Bjarne Stroustrup, Herb Sutter](https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md)

- [Programming Fundamentals - A Modular Structured Approach using C++ - Kenneth Leroy Busbee (PDF)](https://learning.hccs.edu/faculty/ken.busbee/programming-fundamentals-a-modular-structured-approach-using-c)

- [Software Design Using C++ - Br. David Carlson and Br. Isidore Minerd](https://cis.stvincent.edu/html/tutorials/swd/)

- [Think C++: How To Think Like a Computer Scientist - Allen B. Downey (PDF)](https://greenteapress.com/wp/think-c/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#c-2"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#c-2"></cta-button>


## C#

- [Learn C# in Y Minutes](https://learnxinyminutes.com/docs/csharp/)

- [C# Notes for Professionals - Compiled from StackOverflow documentation (PDF)](https://goalkicker.com/CSharpBook/)

- [Data Structures and Algorithms with Object-Oriented Design Patterns in C# - Bruno Preiss](https://web.archive.org/web/20161207142802/http://www.brpreiss.com/books/opus6/)

- [Creating Mobile Apps with Xamarin.Forms C# - Charles Petzold](https://docs.microsoft.com/en-gb/xamarin/xamarin-forms/creating-mobile-apps-xamarin-forms/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#c-1"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#c-1"></cta-button>


## C

- [Let Us C](https://web.archive.org/web/20211006163041/http://pdvpmtasgaon.edu.in/uploads/dptcomputer/Let%20us%20c%20-%20yashwantkanetkar.pdf)

- [Essential C](http://cslibrary.stanford.edu/101/EssentialC.pdf)

- [C Elements of Style](http://www.oualline.com/books.free/style/)

- [Modeling with Data ](https://ben.klemens.org/pdfs/gsl_stats.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#c"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#c"></cta-button>


## Chapel

- [Chapel Tutorial](http://faculty.knox.edu/dbunde/teaching/chapel/)

- [Chapel Tutorial for Programmers](http://web.archive.org/web/20150310075109/http://cs.colby.edu/kgburke/?resource=chapelTutorial)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#chapel"></cta-button>


## Cilk

- [Cilk 5.4.6 Reference Manual (PDF)](https://par.tuwien.ac.at/material/manual-5.4.6.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#cilk"></cta-button>


## Clojure

- [A Brief Beginner’s Guide To Clojure](http://www.unexpected-vortices.com/clojure/brief-beginners-guide/)

- [Clojure - Functional Programming for the JVM - R. Mark Volkmann](https://objectcomputing.com/resources/publications/sett/march-2009-clojure-functional-programming-for-the-jvm)

- [Clojure by Example - Hirokuni Kim](https://kimh.github.io/clojure-by-example/#about-this-page)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#clojure"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#clojure"></cta-button>


## CMake

- [An Introduction to Modern CMake - Henry Schreiner (HTML)](https://cliutils.gitlab.io/modern-cmake/)

- [CMake Tutorial (HTML)](https://cmake.org/cmake/help/latest/guide/tutorial/index.html)

- [Quick CMake tutorial (HTML)](https://www.jetbrains.com/help/clion/quick-cmake-tutorial.html)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#cmake"></cta-button>


## COBOL

- [CMicro Focus: OO Programming with Object COBOL for UNIX (1999) - MERANT International Ltd. (HTML)](https://www.microfocus.com/documentation/object-cobol/oc41books/oppubb.htm)

- [OpenCOBOL 1.1 - Programmer's Guide (PDF)](https://gnucobol.sourceforge.io/guides/OpenCOBOL%20Programmers%20Guide.pdf)

- [ILE COBOL Programmer's Guide (PDF)](https://www.ibm.com/docs/de/ssw_ibm_i_74/pdf/sc092539.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#cobol"></cta-button>


## CoffeeScript

- [CoffeeScript Cookbook](https://coffeescript-cookbook.github.io/)

- [Hard Rock CoffeeScript - Alex Chaplinsky (gitbook)](https://alchaplinsky.github.io/hard-rock-coffeescript/)

- [Smooth CoffeeScript](http://autotelicum.github.io/Smooth-CoffeeScript/SmoothCoffeeScript.html)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#coffeescript"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#coffeescript"></cta-button>


## ColdFusion

- [CFML In 100 Minutes - J. Casimir](https://github.com/mhenke/CFML-in-100-minutes/blob/master/cfml100mins.markdown)

- [Learn CF in a Week](http://learncfinaweek.com/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#coldfusion"></cta-button>


## Component Pascal

- [Computing Fundamentals - Stan Warford (PDF)](https://cslab.pepperdine.edu/warford/ComputingFundamentals/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#component-pascal"></cta-button>


## Cool

- [CoolAid: The Cool 2013 Reference Manual (PDF)](https://www.eecis.udel.edu/~cavazos/cisc672/docs/cool-manual.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#cool"></cta-button>


## Coq

- [Certified Programming with Dependent Types](http://adam.chlipala.net/cpdt/html/toc.html)

- [Software Foundations](https://softwarefoundations.cis.upenn.edu/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#coq"></cta-button>


## Crystal

- [Crystal for Rubyists](https://www.crystalforrubyists.com/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#crystal"></cta-button>


## CUDA

- [CUDA C Best Practices Guide (PDF)](https://docs.nvidia.com/pdf/CUDA_C_Best_Practices_Guide.pdf)

- [CUDA C Programming Guide (PDF)](https://docs.nvidia.com/pdf/CUDA_C_Programming_Guide.pdf)

- [OpenCL Programming Guide for CUDA Architecture (PDF)](https://www.nvidia.com/content/cudazone/download/OpenCL/NVIDIA_OpenCL_ProgrammingGuide.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#cuda"></cta-button>


## D

- [D Templates Tutorial](https://github.com/PhilippeSigaud/D-templates-tutorial)

- [Programming in D](http://ddili.org/ders/d.en/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#d"></cta-button>


## Dart

- [Essential Dart - Krzysztof Kowalczyk and StackOverflow Contributors](https://www.programming-books.io/essential/dart/)

- [Learning Dart - Compiled from StackOverflow documentation (PDF)](https://riptutorial.com/Download/dart.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#dart"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#dart"></cta-button>


## DB2

- [Getting started with DB2 Express-C (PDF)](http://public.dhe.ibm.com/software/dw/db2/express-c/wiki/Getting_Started_with_DB2_Express_v9.7_p4.pdf)

- [Getting started with IBM Data Studio for DB2 (PDF)](http://public.dhe.ibm.com/software/dw/db2/express-c/wiki/Getting_Started_with_IBM_Data_Studio_for_DB2_p3.pdf)

- [Getting started with IBM DB2 development (PDF)](http://public.dhe.ibm.com/software/dw/db2/express-c/wiki/Getting_Started_with_DB2_App_Dev_p2.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#db2"></cta-button>


## DBMS

- [Database Management Systems eBooks For All Edition (PDF)](http://www.lincoste.com/ebooks/english/pdf/computers/database_management_systems.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#dbms"></cta-button>


## Delphi / Pascal

- [Modern Object Pascal Introduction for Programmers - Michalis Kamburelis (AsciiDoc, HTML, PDF)](https://github.com/michaliskambi/modern-pascal-introduction)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#delphi--pascal"></cta-button>

## DTrace

- [IllumOS Dynamic Tracing Guide](http://dtrace.org/guide/preface.html)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#dtrace"></cta-button>


## Eiffel

- [A Functional Pattern System for Object-Oriented Design - Thomas Kuhne (PDF)](https://homepages.ecs.vuw.ac.nz/~tk/fps/fps-sans-escher.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#eiffel"></cta-button>


## Elixir

- [Getting Started Guide (HTML) (PDF, MOBI, EPUB)](http://elixir-lang.org/getting-started/introduction.html)

- [The Ultimate Guide To Elixir For Object-Oriented Programmers - Bruce Park (HTML)](http://www.binarywebpark.com/ultimate-guide-elixir-object-oriented-programmers/)

- [Learning the Elixir Language - Compiled from StackOverflow Documentation (PDF)](https://riptutorial.com/Download/elixir-language.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#elixir"></cta-button>


### Ecto

- [Ecto Getting Started Guide (HTML)](https://hexdocs.pm/ecto/getting-started.html#content)

- [The Little Ecto Cookbook - José Valim, Dashbit (PDF) (email address required)](https://dashbit.co/ebooks/the-little-ecto-cookbook)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#ecto"></cta-button>


### Phoenix

- [Phoenix Framework Guide (HTML)](https://hexdocs.pm/phoenix/overview.html)

- [Versioned APIs with Phoenix - Elvio Vicosa (PDF)](https://elviovicosa.com/freebies/versioned-apis-with-phoenix-by-elvio-vicosa.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#phoenix"></cta-button>


## Emacs

- [An Introduction to Programming in Emacs Lisp](https://www.gnu.org/software/emacs/manual/eintr.html)

- [Emacs for the Modern World (HTML)](https://www.finseth.com/craft/)

- [GNU Emacs Manual](https://www.gnu.org/software/emacs/manual/emacs.html)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#emacs"></cta-button>


## Embedded Systems

- [Control and Embedded Systems (HTML)](https://www.learn-c.com/)

- [Discovering the STM32 Microcontroller (PDF)](https://legacy.cs.indiana.edu/~geobrown/book.pdf)

- [First Steps with Embedded Systems - Byte Craft Limited (PDF)](https://www.phaedsys.com/principals/bytecraft/bytecraftdata/bcfirststeps.pdf)

- [Mastering the FreeRTOS Real Time Kernel - a Hands On Tutorial Guide - (PDF)](https://freertos.org/Documentation/RTOS_book.html)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#embedded-systems"></cta-button>


## Erlang

- [Concurrent Programming in ERLANG (PDF)](https://erlang.org/download/erlang-book-part1.pdf)

- [Learn You Some Erlang For Great Good - Fred Hebert (HTML)](https://learnyousomeerlang.com/)

- [Making reliable distributed systems in the presence of software errors - Joe Armstrong (PDF)](https://erlang.org/download/armstrong_thesis_2003.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#erlang"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#erlang"></cta-button>


## F#

- [Analyzing and Visualizing Data with F# - Tomas Petricek (PDF) (🗃️ archived)](https://web.archive.org/web/20201023042804/https://www.oreilly.com/programming/free/files/analyzing-visualizing-data-f-sharp.pdf)

- [Programming Language Concepts for Software Developers](https://archive.org/details/B-001-003-622)

- [F# Programming - Wikibooks](https://en.wikibooks.org/wiki/F_Sharp_Programming)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#f-sharp"></cta-button>


## Firefox OS

- [Quick Guide For Firefox OS App Development: Creating HTML5 based apps for Firefox OS - Andre Garzia](https://leanpub.com/quickguidefirefoxosdevelopment/read)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#firefox-os"></cta-button>


## Flutter

- [Flutter in Action - Eric Windmill (HTML)](https://livebook.manning.com/book/flutter-in-action/about-this-book/)

- [Flutter Succinctly, Syncfusion (PDF, Kindle)](https://www.syncfusion.com/succinctly-free-ebooks/flutter-succinctly)

- [Flutter Tutorial - Tutorials Point (HTML, PDF)](https://www.tutorialspoint.com/flutter/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#flutter"></cta-button>


## Force.com

- [Force.com Fundamentals (PDF)](http://developerforce.s3.amazonaws.com/books/Force.com_Fundamentals.pdf)

- [Force.com Workbook (PDF)](https://web.archive.org/web/20160804055738/http://resources.docs.salesforce.com:80/sfdc/pdf/forcecom_workbook.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#forcecom"></cta-button>


## Forth

- [A Beginner's Guide to Forth - J.V. Noble](https://web.archive.org/web/20180919061255/http://galileo.phys.virginia.edu/classes/551.jvn.fall01/primer.htm)

- [Easy Forth - Nick Morgan (HTML)](https://skilldrick.github.io/easyforth/)

- [Programming Forth (PDF)](https://www.mpeforth.com/arena/ProgramForth.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#forth"></cta-button>


## Fortran

- [Introduction to Fortran - Sebastian Ehlert, Julius Stückrath, Marcel Mueller, Marcel Stahn (HTML)](https://qc2-teaching.readthedocs.io/en/latest/programming.html)

- [Exploring Modern Fortran Basics - Milan Curcic](https://www.manning.com/books/exploring-modern-fortran-basics)

- [Professional Programmer’s Guide to Fortran77 (2005) - Clive G. Page (PDF)](https://www.star.le.ac.uk/~cgp/prof77.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#fortran"></cta-button>


## FreeBSD

- [Books and Articles from FreeBSD Site](https://www.freebsd.org/docs/books/)

- [The Complete FreeBSD](http://www.lemis.com/grog/Documentation/CFBSD/)

- [Using C on the UNIX System - David A. Curry](https://www.bitsinthewind.com/about-dac/publications/using-c-on-the-unix-system)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#freebsd"></cta-button>


## Git

- [git - the simple guide](http://rogerdudler.github.io/git-guide/)

- [Introduction to Git and Github - Tutorial - Dr. Chris Bourke (PDF)](http://cse.unl.edu/~cbourke/gitTutorial.pdf)

- [Learn Git - Learn Version Control with Git - Tobias Günther](https://www.git-tower.com/learn/git/ebook)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#git"></cta-button>


## Go

- [An Introduction to Programming in Go](https://www.golang-book.com/)

- [Building Web Apps with Go](https://codegangsta.gitbooks.io/building-web-apps-with-go/content/)

- [Practical Cryptography With Go - Kyle Isom](https://leanpub.com/gocrypto/read)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#go"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#go"></cta-button>


## GraphQL

- [Fullstack GraphQL](https://github.com/GraphQLCollege/fullstack-graphql)

- [Learning graphqL (PDF)](https://riptutorial.com/Download/graphql.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#graphql"></cta-button>


## Gradle

- [Building Java Projects with Gradle](https://spring.io/guides/gs/gradle/)

- [Gradle Succinctly - José Roberto Olivas Mendoza](https://www.syncfusion.com/succinctly-free-ebooks/gradle-succinctly)

- [Gradle User Guide - Hans Dockter, Adam Murdoch (PDF)](https://docs.gradle.org/current/userguide/userguide.html)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#gradle"></cta-button>


## Grails

- [Getting Started with Grails](https://www.infoq.com/minibooks/grails-getting-started/)

- [Grails Tutorial for Beginners](https://web.archive.org/web/20210519053040/http://grails.asia/grails-tutorial-for-beginners/)

- [The Grails Framework- (PDF)](http://docs.grails.org/latest/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#grails"></cta-button>


### Spock Framework

- [Spock Framework Reference Documentation - Peter Niederwieser](https://spockframework.org/spock/docs/2.1/index.html)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#spock-framework"></cta-button>


## Hack

- [Hack Documentation](https://docs.hhvm.com/hack/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#hack"></cta-button>


## Hadoop

- [Data-Intensive Text Processing with MapReduce (Jimmy Lin and Chris Dyer) (PDF)](http://lintool.github.io/MapReduceAlgorithms/MapReduce-book-final.pdf)

- [Hadoop for Windows Succinctly - Dave Vickers](https://www.syncfusion.com/succinctly-free-ebooks/hadoop-for-windows-succinctly)

- [Hadoop Illuminated - Mark Kerzner & Sujee Maniyam](http://hadoopilluminated.com/index.html)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#hadoop"></cta-button>


## Haskell

- [Introduction to Haskell - Full course ](https://www.seas.upenn.edu/~cis194/fall16/)

- [Happy Learn Haskell Tutorial](http://www.happylearnhaskelltutorial.com/)

- [Developing Web Applications with Haskell and Yesod - Michael Snoyman](https://www.yesodweb.com/book)

- [The Haskell School of Music - From Signals to Symphonies - Paul Hudak (PDF)](https://www.cs.yale.edu/homes/hudak/Papers/HSoM.pdf)


<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#haskell"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#haskell"></cta-button>


## Haxe

- [Haxe and JavaScript - Matthijs Kamstra (wikibook)](https://matthijskamstra.github.io/haxejs/#/)

- [Haxe Manual - Haxe Foundation (PDF, HTML)](https://haxe.org/documentation/introduction/)

- [HaxeFlixel Handbook (HTML)](https://haxeflixel.com/documentation/haxeflixel-handbook/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#haxe"></cta-button>


## HTML / CSS

- [HTML5 For Web Designers - Jeremy Keith](https://html5forwebdesigners.com/)

- [CSS Animation 101](https://github.com/cssanimation/css-animation-101)

- [Adaptive Web Design - Aaron Gustafson](https://adaptivewebdesign.info/1st-edition/)

- [Learning sass - Compiled from Stack Overflow documentation (PDF)](https://riptutorial.com/Download/sass.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#html--css"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#html--css"></cta-button>


## Icon

- [The Implementation of the Icon Programming Language](https://www2.cs.arizona.edu/icon/ibsale.htm)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#icon"></cta-button>


## iOS

- [Start Developing iOS Apps (Swift) (HTML)](https://developer.apple.com/tutorials/SwiftUI)

- [Start Developing iOS Apps Today (Objective-C) - (PDF)](https://everythingcomputerscience.com/books/RoadMapiOS.pdf)

- [iOS Developer Notes for Professionals - Compiled from StackOverflow Documentation (PDF)](https://goalkicker.com/iOSBook/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#ios"></cta-button>


## IoT

- [IoT in five days- V1.1 (PDF, EPUB)](https://github.com/marcozennaro/IPv6-WSN-book/tree/master/Releases)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#iot"></cta-button>


## Isabelle/HOL

- [Concrete Semantics - A Proof Assistant Approach by Tobias Nipkow and Gerwin Klein (PDF)](https://www21.in.tum.de/~nipkow/Concrete-Semantics/)

- [Isabelle/HOL - A Proof Assistant for Higher-Order Logic by Tobias Nipkow and Lawrence C. Paulson and Markus Wenzel (PDF)](https://isabelle.in.tum.de/doc/tutorial.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#isabellehol"></cta-button>


## J

- [Easy J by Linda Alvord, Norman Thomson (PDF) (Word DOC)](https://www.jsoftware.com/books/pdf/easyj.pdf)

- [J for C Programmers by Henry Rich](https://www.jsoftware.com/help/jforc/contents.htm)

- [Learning J by Roger Stokes- online](https://www.jsoftware.com/help/learning/contents.htm)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#j"></cta-button>


## Java

- [Introduction to Programming in Java - Robert Sedgewick and Kevin Wayne](https://introcs.cs.princeton.edu/java/home/)

- [3D Programming in Java - Daniel Selman (PDF)](http://www.mat.uniroma2.it/~picard/SMC/didattica/materiali_did/Java/Java_3D/Java_3D_Programming.pdf)

- [Java Language and Virtual Machine Specifications - James Gosling, et al](https://docs.oracle.com/javase/specs/)

- [The Java EE6 Tutorial (PDF)](https://docs.oracle.com/javaee/6/tutorial/doc/javaeetutorial6.pdf)

- [The Java EE7 Tutorial - Eric Jendrock, et al (PDF)](https://docs.oracle.com/javaee/7/JEETT.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#java"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#java"></cta-button>


### Codename One

- [Codename One Developer Guide (PDF)](https://www.codenameone.com/files/developer-guide.pdf)

- [Create an Uber Clone in 7 Days (first 2 chapters) - Shai Almog (PDF)](https://uber.cn1.co/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#codename-one"></cta-button>


### Java Reporting

- [The JasperReports Ultimate Guide, Third Edition (PDF)](http://jasperreports.sourceforge.net/JasperReports-Ultimate-Guide-3.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#java-reporting"></cta-button>


### Spring

- [Spring Framework Cookbook: Hot Recipes for Spring Framework - JCGs (Java Code Geeks) (PDF)](https://www.javacodegeeks.com/wp-content/uploads/2017/01/Spring-Framework-Cookbook.pdf)

- [Spring Framework Notes for Professionals - Compiled from StackOverflow documentation (PDF)](https://goalkicker.com/SpringFrameworkBook/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#spring"></cta-button>


### Spring Boot

- [Building modern Web Apps with Spring Boot and Vaadin (PDF)](https://v.vaadin.com/hubfs/Pdfs/Building%20Modern%20Web%20Apps%20with%20Spring%20Boot%20and%20Vaadin.pdf)

- [Spring Boot Reference Guide - Phillip Webb et al. (PDF)](https://docs.spring.io/spring-boot/docs/current/reference/html/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#spring-boot"></cta-button>


### Spring Data

- [Spring Data Reference - Oliver Gierke, Thomas Darimont, Christoph Strobl, Mark Paluch, Jay Bryant](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#spring-data"></cta-button>


### Spring Security

- [Spring Security Reference - Ben Alex, Luke Taylor, Rob Winch](https://docs.spring.io/spring-security/reference/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#spring-security"></cta-button>


### Wicket

- [Official Free Online Guide for Apache Wicket framework](https://wicket.apache.org/learn/#guide)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#wicket"></cta-button>


## JavaScript

- [The JavaScript Beginner's Handbook - Flavio Copes (PDF, EPUB, Kindle) (email address requested)](https://flaviocopes.com/page/javascript-handbook/)

- [Book of Modern Frontend Tooling - Various (HTML)](https://github.com/tooling/book-of-modern-frontend-tooling)

- [JavaScript ES6 and beyond - Alberto Montalesi (PDF, epub)](https://github.com/AlbertoMontalesi/The-complete-guide-to-modern-JavaScript)

- [Single page apps in depth - Mixu (HTML)](http://singlepageappbook.com/)

- [Understanding ECMAScript 6 - Nicholas C. Zakas (HTML)](https://leanpub.com/understandinges6/read)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#javascript"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#javascript"></cta-button>


### AngularJS

- [AngularJS - Step by Logical Step - Nicholas Johnson (HTML)](http://nicholasjohnson.com/blog/angularjs-step-by-logical-step/)

- [AngularJS Notes for Professionals - Compiled from StackOverflow Documentation (PDF)](https://material.angularjs.org/latest/)

- [AngularJS Style Guide for teams - Todd Motto (HTML)](https://github.com/toddmotto/angularjs-styleguide)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#angularjs"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#angularjs"></cta-button>


### Backbone.js

- [A pragmatic guide to Backbone.js apps](http://pragmatic-backbone.com/)

- [Getting Started with Backbone.js](https://code.tutsplus.com/tutorials/getting-started-with-backbonejs--net-19751)

- [How to share Backbone.js models with node.js](https://amirmalik.net/2010/11/27/how-to-share-backbonejs-models-with-nodejs)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#backbonejs"></cta-button>


### Booty5.js

- [The Booty5 HTML5 Game Maker Manual](http://booty5.com/booty5-free-html-game-maker-e-book-manual/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#booty5js"></cta-button>


### D3.js

- [D3 Tips and Tricks - Malcolm Maclean](https://leanpub.com/D3-Tips-and-Tricks/read)

- [Dashing D3.js Tutorial](https://www.dashingd3js.com/d3-tutorial)

- [Interactive Data Visualization with D3](https://alignedleft.com/tutorials/d3)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#d3js"></cta-button>


### Dojo

- [Dojo: The Definitive Guide - Matthew A. Russell](https://www.oreilly.com/library/view/dojo-the-definitive/9780596516482/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#dojo"></cta-button>


### Electron

- [Electron Succinctly, Syncfusion (PDF) ](https://www.syncfusion.com/succinctly-free-ebooks/electron-succinctly)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#electron"></cta-button>


### Elm

- [An Introduction to Elm (HTML)](https://guide.elm-lang.org/)

- [Elm Programming Language  (HTML)](https://en.wikibooks.org/wiki/Elm_programming_language)

- [The Elm Architecture](https://github.com/evancz/elm-architecture-tutorial)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#elm"></cta-button>


### Ember.js

- [DockYard Ember.js Style Guide](https://github.com/DockYard/styleguides/blob/master/engineering/ember.md)

- [Ember App with RailsApi](https://dockyard.com/blog/ember/2013/01/07/building-an-ember-app-with-rails-api-part-1)

- [Ember.js - Getting started](https://guides.emberjs.com/release/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#emberjs"></cta-button>


### Express.js

- [Express.js Guide - Azat Mardanov](https://web.archive.org/web/20140621124403/https://leanpub.com/express/read)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#expressjs"></cta-button>


### Fastify

- [Fastify - Latest Documentation (HTML)](https://www.fastify.io/docs/latest/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#fastify"></cta-button>


### Ionic

- [Ionic 4 Succinctly - Ed Freitas](https://www.syncfusion.com/succinctly-free-ebooks/ionic-4-succinctly)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#ionic"></cta-button>


### jQuery

- [JavaScript Fundamentals, Plus a Dash Of JQuery](http://nicholasjohnson.com/javascript-book/)

- [jQuery Notes for Professionals - Compiled from StackOverflow Documentation (PDF)](https://goalkicker.com/jQueryBook/)

- [jQuery Novice to Ninja (PDF)](http://mediatheque.cite-musique.fr/MediaComposite/Debug/Dossier-Orchestre/ressources/jQuery.Novice.to.Ninja.2nd.Edition.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#jquery"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#jquery"></cta-button>


### meteor

- [Your First Meteor Application, A Complete Beginner’s Guide to the Meteor JavaScript Framework](http://meteortips.com/first-meteor-tutorial/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#meteor"></cta-button>


### Next.js

- [Mastering Next.js](https://masteringnextjs.com/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#nextjs"></cta-button>


### Node.js

- [From Containers to Kubernetes with Node.js - Kathleen Juell (PDF, EPUB)](https://www.digitalocean.com/community/books/from-containers-to-kubernetes-with-node-js-ebook)

- [Full Stack JavaScript: Learn Backbone.js, Node.js and MongoDB - Azat Mardan](https://github.com/azat-co/fullstack-javascript)

- [How To Code in Node.js - eBook ](https://www.digitalocean.com/community/books/how-to-code-in-node-js-ebook)

- [Node.js Tutorial - W3Schools](https://www.w3schools.com/nodejs/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#nodejs"></cta-button>


### Om

- [Om Tutorial](http://awkay.github.io/om-tutorial/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#om"></cta-button>


### React

- [Hacking with React](http://www.hackingwithreact.com/)

- [How To Code in React.js - Joe Morgan](https://www.digitalocean.com/community/books/how-to-code-in-react-js-ebook)

- [React In-depth: An exploration of UI development](https://developmentarc.gitbooks.io/react-indepth/content/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#react"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#react"></cta-button>


### React Native

- [React Native Express](https://www.reactnative.express/)

- [React Native Notes for Professionals - Compiled from StackOverflow documentation (PDF)](https://goalkicker.com/ReactNativeBook/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#react-native"></cta-button>


### Redux

- [Full-Stack Redux Tutorial](https://teropa.info/blog/2015/09/10/full-stack-redux-tutorial.html)

- [SoundCloud Application in React + Redux](https://www.robinwieruch.de/the-soundcloud-client-in-react-redux/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#redux"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#redux"></cta-button>


### Svelte

- [Svelte Tutorial - Svelte.dev](https://svelte.dev/tutorial/basics)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#svelte"></cta-button>


### Vue.js

- [Learning Vue.js (PDF)](https://riptutorial.com/Download/vue-js.pdf)

- [30 Days Of Vue ](https://www.newline.co/30-days-of-vue)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#vuejs"></cta-button>


## Jenkins

- [Jenkins: The Definitive Guide (PDF)](https://www.bogotobogo.com/DevOps/Jenkins/images/Intro_install/jenkins-the-definitive-guide.pdf)

- [Jenkins User Handbook (PDF)](https://www.jenkins.io/user-handbook.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#jenkins"></cta-button>


## Julia

- [Introducing Julia - Wikibooks](https://en.wikibooks.org/wiki/Introducing_Julia)

- [Julia by Example - Samuel Colvin (GitHub repo)](https://juliabyexample.helpmanual.io/)

- [Julia Data Science - Jose Storopoli, Rik Huijzer, and Lazaro Alonso](https://juliadatascience.io/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#julia"></cta-button>


## Kotlin

- [Kotlin Notes for Professionals - Compiled from StackOverflow documentation (PDF)](https://goalkicker.com/KotlinBook/)

- [Kotlin Official Documentation](https://kotlinlang.org/docs/home.html)

- [Learning Kotlin (PDF)](https://riptutorial.com/Download/kotlin.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#kotlin"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#kotlin"></cta-button>


## LaTeX

- [Begin Latex in minutes](https://github.com/luong-komorebi/Begin-Latex-in-minutes)

- [LaTeX - Wikibooks](https://en.wikibooks.org/wiki/LaTeX)

- [LaTex Notes for Professionals - Compiled from StackOverflow documentation (PDF)](https://goalkicker.com/LaTeXBook/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#latex--tex"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#latex"></cta-button>


### TeX

- [Notes On Programming in TeX - Christian Feursänger (PDF)](http://pgfplots.sourceforge.net/TeX-programming-notes.pdf)

- [TeX for the Impatient - Paul Abrahams, Kathryn Hargreaves, and Karl Berry](https://www.gnu.org/software/teximpatient/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#tex"></cta-button>


## Limbo

- [Inferno Programming With Limbo](http://doc.cat-v.org/inferno/books/inferno_programming_with_limbo/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#limbo"></cta-button>

## Linux

- [Linux from Scratch](https://www.linuxfromscratch.org/lfs/view/stable/)

- [Advanced Linux Programming](https://mentorembedded.github.io/advancedlinuxprogramming/)

- [Secure Programming HOWTO - Creating Secure Software - D. A. Wheeler (HTML, PDF)](https://dwheeler.com/)

- [The Linux Kernel Module Programming Guide](https://sysprog21.github.io/lkmpg/)

- [Ubuntu Server Guide (PDF)](https://assets.ubuntu.com/v1/f954307f-ubuntu-server-guide.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#linux"></cta-button>


## Lisp

- [Common Lisp: A Gentle Introduction to Symbolic Computation - David S. Touretzky (PDF, PS)](http://www.cs.cmu.edu/~dst/LispBook/)

- [Google's Common Lisp Style Guide](https://google.github.io/styleguide/lispguide.xml)

- [Paradigms of Artificial Intelligence Programming: Case Studies in Common Lisp - Peter Norvig](https://github.com/norvig/paip-lisp)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#lisp"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#lisp"></cta-button>


## Livecode

- [LiveCode userguide (PDF)](https://www.scribd.com/doc/216789127/LiveCode-userguide)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#livecode"></cta-button>


## Lua

- [Lua Programming - Wikibooks](https://en.wikibooks.org/wiki/Lua_Programming)

- [Lua Tutorial - Tutorials Point (HTML, PDF)](https://www.tutorialspoint.com/lua/)

- [Programming in Lua (first edition)](https://www.lua.org/pil/contents.html)


<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#lua"></cta-button>


## Make

- [Makefile tutorial - Chase Lambert](https://makefiletutorial.com/)

- [Managing Projects with GNU Make - Robert Mecklenburg](https://www.oreilly.com/openbook/make3/book/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#make"></cta-button>


## Markdown

- [Learn Markdown - Sammy P., Aaron O. (PDF) (EPUB) (MOBI)](https://www.gitbook.com/?utm_source=legacy&utm_medium=redirect&utm_campaign=close_legacy)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#markdown"></cta-button>


## Mathematica

- [Vector Math for 3d Computer Graphics](https://chortle.ccsu.edu/VectorLessons/index.html)

- [Mathematica® programming: an advanced introduction by Leonid Shifrin](http://www.mathprogramming-intro.org/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#mathematica"></cta-button>


## MATLAB

- [An Interactive Introduction to MATLAB (PDF)](http://www.science.smith.edu/~jcardell/Courses/EGR326/Intro-to-MATLAB.pdf)

- [MATLAB - A Fundamental Tool for Scientific Computing and Engineering Applications - Volume 1](http://www.intechopen.com/books/matlab-a-fundamental-tool-for-scientific-computing-and-engineering-applications-volume-1)

- [Numerical Computing with MATLAB](https://www.mathworks.com/moler/index_ncm.html?w.mathworks.com&nocookie=true)

- [MATLAB Programming - Wikibooks](https://en.wikibooks.org/wiki/MATLAB_Programming)

- [Scientific Computing - Jeffrey R. Chasnov (PDF)](https://www.math.hkust.edu.hk/~machas/scientific-computing.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#matlab"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#matlab"></cta-button>


## Maven

- [Developing with Eclipse and Maven](https://books.sonatype.com/m2eclipse-book/reference/index.html)

- [Maven by Example](https://books.sonatype.com/mvnex-book/reference/public-book.html)

- [Maven: The Complete Reference](https://books.sonatype.com/mvnref-book/reference/public-book.html)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#maven"></cta-button>


## Mercurial

- [Hg Init: a Mercurial Tutorial - Joel Spolsky](https://hginit.github.io/)

- [Mercurial: The Definitive Guide](http://hgbook.red-bean.com/)

- [Mercurial: The Definitive Guide 2nd edition](https://book.mercurial-scm.org/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#mercurial"></cta-button>


## Mercury

- [The Mercury Users' Guide (PDF)](https://mercurylang.org/information/doc-release/user_guide.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#mercury"></cta-button>


## Modelica

- [Modelica by Example](https://book.xogeny.com/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#lua"></cta-button>


## MongoDB

- [Introduction to MongoDB - Tutorials Point (HTML, PDF)](https://www.tutorialspoint.com/mongodb/)

- [MongoDB Koans](https://github.com/chicagoruby/MongoDB_Koans)

- [MongoDB Notes for Professionals - Compiled from StackOverflow Documentation (PDF)](https://goalkicker.com/MongoDBBook/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#mongodb"></cta-button>


## MySQL

- [MySQL 8.0 Tutorial Excerpt (HTML) (PDF)](https://dev.mysql.com/doc/mysql-tutorial-excerpt/8.0/en/tutorial.html)

- [MySQL Notes for Professionals - Compiled from StackOverflow Documentation (PDF)](https://goalkicker.com/MySQLBook/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#mysql"></cta-button>


## Neo4J

- [Graph Algorithms: Practical Examples in Apache Spark and Neo4j ](https://neo4j.com/product/graph-data-science/#graph-data-science-resources)

- [Graph Databases For Dummies ](https://neo4j.com/graph-databases-for-dummies/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#neo4j"></cta-button>


## .NET Core / .NET

- [Clean Code .NET](https://github.com/thangchung/clean-code-dotnet)

- [Entity Framework Core Succinctly - Ricardo Peres](https://www.syncfusion.com/succinctly-free-ebooks/entity-frame-work-core-succinctly)

- [.NET documentation - Microsoft Docs](https://docs.microsoft.com/en-us/dotnet/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#net-core"></cta-button>


## .NET Framework

- [Cryptography in .NET Succinctly - Dirk Strauss](https://www.syncfusion.com/succinctly-free-ebooks/cryptography-in-net-succinctly)

- [Entity Framework](https://weblogs.asp.net/zeeshanhirani/my-christmas-present-to-the-entity-framework-community)

- [Game Creation with XNA - Wikibooks](https://en.wikibooks.org/wiki/Game_Creation_with_XNA)

- [Under the Hood of .NET Memory Management](https://assets.red-gate.com/community/books/under-the-hood-of-net-memory-management.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#net-framework"></cta-button>


## Nim

- [Computer Programming with the Nim Programming Language - Dr. Stefan Salewski](https://ssalewski.de/nimprogramming.html)

- [Nim Basics - narimiran](https://narimiran.github.io/nim-basics/)

- [Nim by Example - Flaviu Tamas](https://nim-by-example.github.io/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#nim"></cta-button>


## NoSQL

- [NoSQL Databases - Christof Strauch (PDF)](https://www.christof-strauch.de/nosqldbs.pdf)

- [Extracting Data from NoSQL Databases: A Step towards Interactive Visual Analysis of NoSQL Data - Petter Nasholm (PDF)](https://publications.lib.chalmers.se/records/fulltext/155048.pdf)

- [How To Manage a Redis Database - Mark Drake (PDF, EPUB)](https://www.digitalocean.com/community/books/how-to-manage-a-redis-database-ebook)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#nosql"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#nosql"></cta-button>


## Oberon
- [Algorithms and Data-Structures - Niklaus Wirth (PDF)](https://people.inf.ethz.ch/wirth/AD.pdf)

- [Object-Oriented Programming in Oberon-2 - Hanspeter Mössenböck (PDF)](https://ssw.jku.at/Research/Books/Oberon2.pdf)

- [Programming in Oberon - Niklaus Wirth (PDF)](https://people.inf.ethz.ch/wirth/ProgInOberonWR.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#oberon"></cta-button>


## Objective-C

- [Google's Objective-C Style Guide](https://github.com/google/styleguide/blob/gh-pages/objcguide.md)

- [Object-Oriented Programming with Objective-C](https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/OOP_ObjC/Introduction/Introduction.html#//apple_ref/doc/uid/TP40005149)

- [Objective-C Notes for Professionals - Compiled from StackOverflow Documentation (PDF)](https://goalkicker.com/ObjectiveCBook/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#objective-c"></cta-button>


## OCaml

- [Developing Applications With Objective Caml](https://caml.inria.fr/pub/docs/oreilly-book/)

- [Functional Programming in OCaml - Michael R. Clarkson](https://cs3110.github.io/textbook/cover.html)

- [Real World OCaml](https://dev.realworldocaml.org/toc.html)

- [Using, Understanding, and Unraveling The OCaml Language: From Practice to Theory and vice versa - Didier Rémy](http://pauillac.inria.fr/~remy/cours/appsem/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#ocaml"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#ocam"></cta-button>


## Octave

- [Octave Programming - Wikibooks](https://en.wikibooks.org/wiki/Octave_Programming_Tutorial)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#octave"></cta-button>


## Odin

- [Overview | Odin Programming Language](https://odin-lang.org/docs/overview/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#odin"></cta-button>


## OpenMP

- [A Guide To OpenMP](https://bisqwit.iki.fi/story/howto/openmp/)

- [OpenMP Application Programming Interface Standard Version 4.0 (PDF)](https://www.openmp.org/wp-content/uploads/OpenMP4.0.0.pdf)

- [OpenMP Application Programming Interface Standard Version 5.0 (PDF)](https://www.openmp.org/wp-content/uploads/OpenMP-API-Specification-5.0.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#openmp"></cta-button>


## OpenSCAD

- [OpenSCAD User Manual - Wikibooks](https://en.wikibooks.org/wiki/OpenSCAD_User_Manual)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#openscad"></cta-button>


## Perl

- [Beginning Perl](https://www.perl.org/books/beginning-perl/)

- [Perl for the Web - C. Radcliff](http://www.globalspin.com/thebook/)

- [Web Client Programming with Perl](https://www.oreilly.com/openbook/webclient/)

- [Exploring Programming Language Architecture in Perl](http://www.billhails.net/Book/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#perl"></cta-button>


## PHP

- [Clean Code PHP](https://github.com/jupeter/clean-code-php)

- [PHP 5 Power Programming (PDF)](https://ptgmedia.pearsoncmg.com/images/013147149X/downloads/013147149X_book.pdf)

- [PHP Best Practices](https://phpbestpractices.org/)

- [Practical PHP Testing](https://www.giorgiosironi.com/2009/12/practical-php-testing-is-here.html)

- [Survive The Deep End: PHP Security](https://phpsecurity.readthedocs.io/en/latest/)

- [PHP Security Guide](https://privacyaustralia.net/phpsec/projects/guide/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#php"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#php"></cta-button>


### CakePHP

- [CakePHP Cookbook 2.x (PDF)](https://book.cakephp.org/2/_downloads/en/CakePHPCookbook.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#cakephp"></cta-button>


### CodeIgniter

- [CodeIgniter 3 User Guide](https://codeigniter.com/userguide3/index.html)

- [CodeIgniter 4 User Guide](https://codeigniter.com/user_guide/index.html)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#codeigniter"></cta-button>


### Drupal

- [The Tiny Book of Rules](https://www.drupal.org/files/tiny-book-of-rules.pdf) (PDF)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#drupal"></cta-button>


### Laravel

- [100 (and counting) Laravel Quick Tips - Povilas Korop / LaravelDaily Team (PDF)](https://laraveldaily.com/wp-content/uploads/2020/04/laravel-tips-2020-04.pdf)

- [Laravel Best Practices](http://www1.laravelbestpractices.com/?tm=1&subid4=1650652164.0480500000&kw=Laravel+Framework+LMS&KW1=Laravel%20Online%20Courses&KW2=Laravel%20Software%20Testing%20Tools&KW3=Laravel%20Security%20Training&searchbox=0&domainname=0&backfill=0)

- [Laravel: Code Bright - Dayle Rees](https://daylerees.com/codebright/)

- [Laravel: Code Happy - Dayle Rees](https://daylerees.com/codehappy/)

- [Laravel: Code Smart - Dayle Rees](https://daylerees.com/codesmart/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#laravel"></cta-button>


### Symfony

- [Symfony 5: The Fast Track](https://symfony.com/doc/5.0/the-fast-track/en/index.html)

- [The Symfony Best practices 4.1.x (PDF)](https://web.archive.org/web/20181017123206/https://symfony.com/pdf/Symfony_best_practices_4.1.pdf)

- [The Symfony Book 4.4.x](https://symfony.com/doc/4.4/index.html)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#symfony"></cta-button>


### Zend

- [Using Zend Framework 3](https://olegkrivtsov.github.io/using-zend-framework-3-book/html/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#zend"></cta-button>


## PicoLisp

- [PicoLisp by Example](https://github.com/tj64/picolisp-by-example)

- [PicoLisp Works](https://github.com/tj64/picolisp-works)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#picolisp"></cta-button>


## PostgreSQL

- [Postgres Official Documentation](https://www.postgresql.org/docs/)

- [PostgreSQL Tutorial - Tutorials Point (HTML, PDF)](https://www.tutorialspoint.com/postgresql/)

- [The Internals of PostgreSQL for database administrators and system developers](http://www.interdb.jp/pg/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#postgresql"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#postgresql"></cta-button>


## PowerShell

- [Learn PowerShell in Y Minutes](https://learnxinyminutes.com/docs/powershell/)

- [PowerShell Notes for Professionals](https://goalkicker.com/PowerShellBook/)

- [Mastering PowerShell v2](https://community.idera.com/database-tools/powershell/powertips/b/ebookv2#pi619PostSortOrder=Ascending)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#powershell"></cta-button>


## Processing

- [The Nature of Code: Simulating Natural Systems with Processing](https://natureofcode.com/book/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#processing"></cta-button>


## Prolog

- [Prolog - complete course](https://www.let.rug.nl/bos/lpn//lpnpage.php?pageid=online)

- [Logic, Programming and Prolog (2ed)](https://www.ida.liu.se/~ulfni53/lpp/)

- [Prolog for Programmers ](https://sites.google.com/site/prologforprogrammers/)

- [Simply Logical: Intelligent Reasoning by Example ](https://book.simply-logical.space/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#prolog"></cta-button>


### Constraint Logic Programming

- [A Gentle Guide to Constraint Logic Programming via ECLiPSe](http://anclp.pl/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#constraint-logic-programming-extended-prolog"></cta-button>


## PureScript

- [PureScript By Example - Phil Freeman](https://leanpub.com/purescript/read)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#purescript"></cta-button>


## Python

- [Beej's Guide to Python Programming - For Beginners - Brian "Beej Jorgensen" Hall (HTML,PDF)](https://beej.us/guide/bgpython/)

- [A Practical Introduction to Python Programming - Brian Heinold (HTML, PDF, Exercises sources)](https://www.brianheinold.net/python/python_book.html)

- [20 Python Libraries You Aren't Using (But Should) - Caleb Hattingh](https://www.oreilly.com/content/20-python-libraries-you-arent-using-but-should/)

- [A Guide to Python's Magic Methods - Rafe Kettler](https://github.com/RafeKettler/magicmethods)

- [Automate the Boring Stuff with Python, 2nd Edition: Practical Programming for Total Beginners](https://automatetheboringstuff.com/2e/chapter0/)

- [Data Structures and Algorithms in Python - B. R. Preiss (PDF)](https://web.archive.org/web/20161016153130/http://www.brpreiss.com/books/opus7/html/book.html)

- [Full Stack Python - Matt Makai](https://www.fullstackpython.com/)

- [Making Games with Python & Pygame - Al Sweigart (3.2)](https://inventwithpython.com/pygame/chapters/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#python"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#python"></cta-button>


### Django

- [Django Official Documentation (PDF) (3.2)](https://buildmedia.readthedocs.org/media/pdf/django/1.10.x/django.pdf)

- [Django Web Framework (Python)](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django)

- [Effective Django (1.5)](https://web.archive.org/web/20181130092020/http://www.effectivedjango.com/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#django"></cta-button>


### Flask

- [Explore Flask - Robert Picard](http://exploreflask.com/en/latest/)

- [Flask Documentation - Pallets](https://flask.palletsprojects.com/en/2.1.x/)

- [Python Flask Tutorial - Tutorials Point (HTML, PDF)](https://www.tutorialspoint.com/flask/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#flask"></cta-button>


### Kivy

- [Kivy Programming Guide](https://kivy.org/doc/stable/guide-index.html)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#kivy"></cta-button>


### Pandas

- [Learn Pandas - Hernan Rojas (0.18.1)](https://bitbucket.org/hrojas/learn-pandas/src/master/)

- [pandas: powerful Python data analysis toolkit - Wes McKinney and the Pandas Development Team (HTML, PDF)](https://pandas.pydata.org/docs/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#pandas"></cta-button>


### Pyramid

- [Quick Tutorial for Pyramid](https://docs.pylonsproject.org/projects/pyramid/en/latest/quick_tutorial/index.html#quick-tutorial)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#pyramid"></cta-button>


### Tornado

- [Learn Web Programming](https://bitbucket.org/hrojas/learn-web-programming/src/master/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#tornado"></cta-button>


## R

- [An Introduction to R -David M. Smith and William N. Venables](https://cran.r-project.org/doc/manuals/R-intro.html)

- [Efficient R programming - Colin Gillespie, Robin Lovelace](https://csgillespie.github.io/efficientR/)

- [Introduction to Probability and Statistics Using R - G. Jay Kerns (PDF)](https://github.com/gjkerns/IPSUR)

- [R Notes for Professionals - Compiled from StackOverflow Documentation (PDF)](https://goalkicker.com/RBook/)

- [R Packages - Hadley Wickham](https://r-pkgs.org/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#r"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#r"></cta-button>


## Racket

- [How to Design Programs - Matthias Felleisen, Robert Bruce Findler, Matthew Flatt, Shriram Krishnamurthi](https://htdp.org/2019-02-24/)

- [Programming Languages: Application and Interpretation](http://cs.brown.edu/courses/cs173/2012/book/index.html)

- [The Racket Guide](https://docs.racket-lang.org/guide/index.html)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#racket"></cta-button>


## Raku

- [X=Raku - Learn X in Y minutes (HTML)](https://learnxinyminutes.com/docs/raku/)

- [Raku Programming - Wikibooks (HTML)](https://en.wikibooks.org/wiki/Raku_Programming)

- [Think Raku  - Laurent Rosenfeld, with Allen B. Downey (PDF](https://github.com/LaurentRosenfeld/think_raku/raw/master/PDF/think_raku.pdf))

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#raku"></cta-button>


## Raspberry Pi

- [Raspberry Pi: Measure, Record, Explore - Malcolm Maclean (HTML)](https://leanpub.com/RPiMRE/read)

- [Raspberry Pi Users Guide - (2012) - Eben Upton (PDF)](https://www.cs.unca.edu/~bruce/Fall14/360/RPiUsersGuide.pdf)

- [The Official Raspberry Pi Project Book 1 (2015) (PDF)](https://magpi.raspberrypi.com/books/projects-1)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#raspberry-pi"></cta-button>


## Ruby

- [Introduction to Programming with Ruby - Launch School](https://launchschool.com/books/ruby)

- [Developing Games With Ruby - Tomas Varaneckas](https://leanpub.com/developing-games-with-ruby/read)

- [Ruby Web Dev: The Other Way - Yevhen Kuzminov](https://leanpub.com/rwdtow/read)

- [Ruby Notes for Professionals - Compiled from StackOverflow Documentation (PDF)](https://goalkicker.com/RubyBook/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#ruby"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#ruby"></cta-button>


### RSpec

- [Better Specs (RSpec Guidelines with Ruby)](https://www.betterspecs.org/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#rspec"></cta-button>


### Ruby on Rails

- [Ruby on Rails 3.2 - Step by Step](http://www.xyzpub.com/en/ruby-on-rails/3.2/)

- [Building REST APIs with Rails - Abraham Kuri Vargas](https://www.softcover.io/read/06acc071/api_on_rails)

- [Ruby on Rails Tutorial (Rails 5): Learn Web Development with Rails - Michael Hartl](https://www.railstutorial.org/book)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#ruby-on-rails"></cta-button>


### Sinatra

- [Sinatra Book](https://github.com/sinatra/sinatra-book)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#sinatra"></cta-button>


## Rust

- [A Gentle Introduction To Rust - Steve J Donovan](https://stevedonovan.github.io/rust-gentle-intro/)

- [Rust by Example](https://doc.rust-lang.org/stable/rust-by-example/)

- [Asynchronous Programming in Rust](https://rust-lang.github.io/async-book/)

- [Rust For Systems Programmers - Nick Cameron](https://github.com/nrc/r4cppp)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#rust"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#rust"></cta-button>


## Sage

- [Sage for Power Users - William Stein (PDF)](https://wstein.org/books/sagebook/sagebook.pdf)

- [The Sage Manuals](https://doc.sagemath.org/html/en/index.html)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#sage"></cta-button>


## Scala

- [A Scala Tutorial for Java programmers (PDF)](https://docs.scala-lang.org/tutorials/scala-for-java-programmers.html)

- [Pro Scala: Monadic Design Patterns for the Web](https://github.com/leithaus/XTrace/tree/monadic/src/main/book/content/)

- [Scala & Design Patterns: Exploring Language Expressivity - Fredrik Skeel Løkke (PDF)](https://www.scala-lang.org/old/sites/default/files/FrederikThesis.pdf)

- [Programming in Scala, First Edition - by M. Odersky, L. Spoon, B. Venners](https://www.artima.com/pins1ed/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#scala"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#scala"></cta-button>


### Lift

- [Exploring Lift (published earlier as "The Definitive Guide to Lift", PDF)](https://exploring.liftweb.net/download.html)

- [Lift](https://github.com/tjweir/liftbook)

- [Lift Cookbook - Richard Dallaway](https://www.oreilly.com/library/view/lift-cookbook/9781449365042/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#lift"></cta-button>


### Play Scala

- [Essential Play - Dave Gurnell (PDF, HTML, EPUB)](https://underscore.io/books/essential-play/)

- [Play Framework Recipes - Alvin Alexander](https://alvinalexander.com/scala/scala-cookbook-play-framework-recipes-pdf-ebook/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#play-scala"></cta-button>


## Scheme

- [An Introduction to Scheme and its Implementation](http://www.cs.rpi.edu/academics/courses/fall00/ai/scheme/reference/schintro-v14/schintro_toc.html)

- [Simply Scheme: Introducing Computer Science - B. Harvey, M. Wright](https://people.eecs.berkeley.edu/~bh/ss-toc2.html)

- [Write Yourself a Scheme in 48 Hours - Wikibooks](https://en.wikibooks.org/wiki/Write_Yourself_a_Scheme_in_48_Hours)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#scheme"></cta-button>


## Scilab

- [Introduction to Scilab](http://forge.scilab.org/index.php/p/docintrotoscilab/downloads/)

- [Programming in Scilab](http://forge.scilab.org/index.php/p/docprogscilab/downloads/)

- [Writing Scilab Extensions](http://forge.scilab.org/index.php/p/docsciextensions/downloads/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#scilab"></cta-button>


## Scratch

- [An Introductory Computing Curriculum Using Scratch](https://creativecomputing.gse.harvard.edu/guide/curriculum.html)

- [Computer Science Concepts in Scratch](https://stwww1.weizmann.ac.il/scratch/scratch_en/)

- [Learn to Code with Scratch - The MagPi magazine (PDF)](https://magpi.raspberrypi.com/books/essentials-scratch-v1)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#scratch"></cta-button>


## Sed

- [GNU sed - Sundeep Agarwal](https://learnbyexample.github.io/learn_gnused/)

- [Sed - An Introduction and Tutorial - Bruce Barnett](https://www.grymoire.com/Unix/Sed.html)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#sed"></cta-button>


## Self

- [The Self Handbook](https://handbook.selflanguage.org/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#self"></cta-button>


## Smalltalk

- [Deep into Pharo - Alexandre Bergel, Damien Cassou, Stéphane Ducasse, Jannik Laval](http://books.pharo.org/deep-into-pharo/)

- [Dynamic Web Development with Seaside - S. Ducasse, L. Renggli, C. D. Shaffer, R. Zaccone](https://book.seaside.st/book)

- [Pharo by Example - Andrew P. Black et al. (Smalltalk Implementation and IDE)](http://books.pharo.org/pharo-by-example/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#smalltalk"></cta-button>


## Snap

- [Snap! Reference Manual - B. Harvey, J. Mönig (PDF)](https://snap.berkeley.edu/snap/snap.html)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#snap"></cta-button>


## Solidity

- [Introductory guide for Solidity - Tutorials Point (HTML)](https://www.tutorialspoint.com/solidity/)

- [The Solidity Reference Guide](https://docs.soliditylang.org/en/v0.8.13/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#solidity"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#solidity"></cta-button>


## SQL (implementation agnostic)

- [Developing Time-Oriented Database Applications in SQL - Richard T. Snodgrass (PDF)](https://www2.cs.arizona.edu/~rts/tdbbook.pdf)

- [Introduction to SQL - Bobby Iliev (Markdown, PDF)](https://github.com/bobbyiliev/introduction-to-sql)

- [SQL For Web Nerds](http://philip.greenspun.com/sql/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#sql-implementation-agnostic"></cta-button>

<cta-button text="More Resources" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#sql"></cta-button>


## SQL Server

- [Best of SQLServerCentral.com Vol 7 *(RedGate, By SQLServerCentral Authors)](https://www.red-gate.com/library/the-best-of-sqlservercentral-com-vol-7)

- [Defensive Database Programming - Alex Kuznetsov (PDF)](https://www.red-gate.com/library/defensive-database-programming)

- [Mastering SQL Server Profiler - Brad McGehee (PDF)](https://www.red-gate.com/library/mastering-sql-server-profiler)

- [Protecting SQL Server Data - John Magnabosco (PDF)](https://www.red-gate.com/library/protecting-sql-server-data)

- [How to Become an Exceptional DBA, Second edition - Brad McGehee (PDF)](https://www.red-gate.com/library/how-to-become-an-exceptional-dba-2nd-edition)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#sql-server"></cta-button>


## Standard ML

- [Programming in Standard ML '97 - Stephen Gilmore, University of Edinburgh](https://homepages.inf.ed.ac.uk/stg/NOTES/)

- [Unix System Programming with Standard ML - Anthony L. Shipman (PDF)](http://mlton.org/References.attachments/Shipman02.pdf)

- [ML for the Working Programmer, 2nd Edition - Lawrence C. Paulson](https://www.cl.cam.ac.uk/~lp15/MLbook/pub-details.html)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#standard-ml"></cta-button>


## Subversion

- [Subversion Version Control (PDF)](https://ptgmedia.pearsoncmg.com/images/0131855182/downloads/Nagel_book.pdf)

- [Version Control with Subversion](https://svnbook.red-bean.com/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#subversion"></cta-button>


## Swift

- [Essential Swift - Krzysztof Kowalczyk (Compiled from StackOverflow Documentation)](https://www.programming-books.io/essential/swift/)

- [The Swift Programming Language (HTML)](https://docs.swift.org/swift-book/index.html)

- [Swift Notes for Professionals - Compiled from StackOverflow Documentation (PDF)](https://goalkicker.com/SwiftBook/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#swift"></cta-button>


### Vapor

- [Vapor 3 Tutorial For Beginners - Tibor Bödecs](https://theswiftdev.com/beginners-guide-to-server-side-swift-using-vapor-4/)

- [Vapor Official Docs](https://docs.vapor.codes/4.0/)


<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#vapor"></cta-button>


## Tcl

- [Tcl Programming - Richard.Suchenwirth, et. al.](https://en.wikibooks.org/wiki/Tcl_Programming)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#tcl"></cta-button>


## TEI

- [What is the Text Encoding Initiative? - Lou Bernard](https://books.openedition.org/oep/426)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#tei"></cta-button>


## Teradata

- [Teradata Books](https://docs.teradata.com/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#teradata"></cta-button>


## Tizen

- [Guide to Developing Tizen Native Application - Jung, Dong-Geun (Denis.Jung) (PDF)](https://developer.tizen.org/sites/default/files/documentation/guide_to_developing_tizen_native_application_en_2.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#tizen"></cta-button>


## TLA

- [Specifying Systems: The TLA+ Language and Tools for Hardware and Software Engineers - Leslie Lamport (Postscript or PDF)](http://lamport.azurewebsites.net/tla/book.html)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#tla"></cta-button>


## TypeScript

- [Learn TypeScript in Y Minutes](https://learnxinyminutes.com/docs/typescript/)

- [Tackling TypeScript: Upgrading from JavaScript - Dr. Axel Rauschmayer](https://exploringjs.com/tackling-ts/toc.html)

- [TypeScript for C# Programmers](https://www.infoq.com/minibooks/typescript-c-sharp-programmers/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#typescript"></cta-button>


### Angular

- [Angular 2 Style Guide - John Papa (HTML)](https://github.com/johnpapa/angular-styleguide/blob/master/a2/README.md)

- [Angular Tutorial (HTML)](https://angular.io/tutorial)

- [Build a Full-Stack Web Application Using Angular & Firebase - Ankit Sharma (PDF)](https://www.c-sharpcorner.com/ebooks/build-a-full-stack-web-application-using-angular-and-firebase)[ - 📦 code samples](https://github.com/AnkitSharma-007/blogging-app-with-Angular-CloudFirestore)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#angular"></cta-button>


### Deno

- [Deno Manual](https://deno.land/manual)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#deno"></cta-button>


## Unix

- [UNIX Tutorial for Beginners](http://www.ee.surrey.ac.uk/Teaching/Unix/)

- [An Introduction to Unix](https://www.oliverelliott.org/article/computing/tut_unix/)

- [Beej's Guide to Unix Interprocess Communication - Brian "Beej Jorgensen" Hall (HTML,PDF)](https://beej.us/guide/bgipc/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#unix"></cta-button>


## Verilog

- [Verilog, Formal Verification and Verilator Beginner's Tutorial - Daniel E. Gisselquist, Ph.D.](https://zipcpu.com/tutorial/)

- [Verilog Quick Reference Guide - Sutherland HDL (PDF)](https://sutherland-hdl.com/pdfs/verilog_2001_ref_guide.pdf)

- [Verilog Tutorial](http://www.asic-world.com/verilog/veritut.html)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#verilog"></cta-button>


## VHDL

- [Free Range VHDL - Bryan Mealy, Fabrizio Tappero (TeX and PDF)](https://github.com/fabriziotappero/Free-Range-VHDL-book)

- [VHDL Tutorial](https://www.seas.upenn.edu/~ese171/vhdl/vhdl_primer.html)

- [VHDL Tutorial: Learn By Example](http://esd.cs.ucr.edu/labs/tutorial/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#vhdl"></cta-button>


## Vim

- [A Byte of Vim](https://vim.swaroopch.com/)

- [Learn Vim Progressively](http://yannesposito.com/Scratch/en/blog/Learn-Vim-Progressively/)

- [Vim Recipes (PDF)](https://web.archive.org/web/20130302172911/http://vim.runpaint.org/vim-recipes.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#vim"></cta-button>


## Visual Basic

- [Visual Basic .NET Notes for Professionals - Compiled from StackOverflow Documentation (PDF)](https://goalkicker.com/VisualBasic_NETBook/)

- [Visual Basic Official Docs](https://docs.microsoft.com/en-us/dotnet/visual-basic/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#visual-basic"></cta-button>


## Visual Prolog

- [A Beginners' Guide to Visual Prolog](https://wiki.visual-prolog.com/index.php?title=A_Beginners_Guide_to_Visual_Prolog&title=A_Beginners_Guide_to_Visual_Prolog)

- [Visual Prolog for Tyros](https://wiki.visual-prolog.com/index.php?title=Visual_Prolog_for_Tyros&title=Visual_Prolog_for_Tyros)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#visual-prolog"></cta-button>


## Vulkan

- [Vulkan Tutorial - Alexander Overvoorde (EPUB, HTML, PDF) (C++)](https://vulkan-tutorial.com/)

- [Vulkan Tutorial Java - Cristian Herrera et al (Java)](https://github.com/Naitsirc98/Vulkan-Tutorial-Java)

- [Vulkano - Tomaka et al. (HTML) (Rust)](https://vulkano.rs/guide/introduction)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#vulkan"></cta-button>


## Web Services

- [RESTful Web Services (PDF)](http://restfulwebapis.org/RESTful_Web_Services.pdf)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#web-services"></cta-button>


## Windows 8

- [Programming Windows Store Apps with HTML, CSS, and JavaScript, Second Edition](https://web.archive.org/web/20150624142410/http://download.microsoft.com/download/6/6/5/665AF7A6-2184-45DC-B9DA-C89185B01937/Microsoft_Press_eBook_Programming_Windows_8_Apps_HTML_CSS_JavaScript_2E_PDF.pdf) - Kraig Brockschmidt (PDF)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#windows-8"></cta-button>


## Windows Phone

-  [Developing An Advanced Windows Phone 7.5 App That Connects To The Cloud](https://web.archive.org/web/20150709045622/http://download.microsoft.com/download/C/4/6/C4635738-5E06-4DF7-904E-BDC22AED2E58/Developing%20an%20Advanced%20Windows%20Phone%207.5%20App%20that%20Connects%20to%20the%20Cloud.pdf) - MSDN Library, David Britch, Francis Cheung, Adam Kinney, Rohit Sharma (PDF) (:card_file_box: *archived*)

- [Windows Phone 8 Development Succinctly - Matteo Pagani (PDF)](https://www.syncfusion.com/succinctly-free-ebooks/windowsphone8)

- [Windows Phone 8.1 Development for Absolute Beginners](https://docs.microsoft.com/en-gb/shows/)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#windows-phone"></cta-button>


## Workflow

- [Declare Peace on Virtual Machines. A guide to simplifying vm-based development on a Mac](https://leanpub.com/declarepeaceonvms/read)

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#workflow"></cta-button>


## xBase (dBase / Clipper / Harbour)

- [Application Development with Harbour - Wikibooks](https://en.wikibooks.org/wiki/Application_Development_with_Harbour)

- [CA-Clipper 5.2 Norton Guide](https://web.archive.org/web/20190516192814/http://www.ousob.com/ng/clguide/)

- [Clipper Tutorial: a Guide to Open Source Clipper(s) - Wikibooks](https://en.wikibooks.org/wiki/Clipper_Tutorial%3A_a_Guide_to_Open_Source_Clipper(s))

<cta-button text="More Books" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#xbase-dbase--clipper--harbour"></cta-button>






